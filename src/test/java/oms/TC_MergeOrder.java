package oms;

import apis.oms.Oms;
import object.BaseTest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_MergeOrder extends BaseTest {
    public TC_MergeOrder(){
        super.sheetName = "Merge_Order";
    }

    @Parameters("refs")
    @Test
    public void TestCreate(@Optional("All") String reference) {
        Oms oms = new Oms();
        JSONObject data = new JSONObject();
        String orderNumber1;
        String orderNumber2;

        for(int i=1; i <= 1; i++){
            String orderNumber = XLSWorker.getValueFromExcel(i, "OrderNumber", sheetName);
            String expectOrderDetail = XLSWorker.getValueFromExcel(i, "ExpectOrderDetail", sheetName);
            JSONArray array = new JSONArray(expectOrderDetail);

            if(orderNumber.equals("GetOrderRandom")){
                // Bước 1: Tạo đơn hàng 1
                oms.run(11, "", "Create_Order", null);
                orderNumber1 = oms.getOrderNumber();
                //orderNumber1 = "14219223822";
                data.put("orderNumber1", orderNumber1);
                ((JSONObject) array.get(0)).put("OrderNumber", orderNumber1);

                // Bước 2: Tạo đơn hàng 2
                oms.resetOrderNumber();
                oms.run(12, "", "Create_Order", null);
                orderNumber2 = oms.getOrderNumber();
                //orderNumber2 = "14219223825";
                data.put("orderNumber2", orderNumber2);
                ((JSONObject) array.get(1)).put("OrderNumber", orderNumber2);
            }else{
                data.put("orderNumber", orderNumber);
                ((JSONObject) array.get(2)).put("OrderNumber", orderNumber);
            }

            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                // Bước 3: Merge đơn hàng
                oms.resetOrderNumber();
                String response = oms.run(i, "", sheetName, data);
                boolean result = oms.verifyResponse(i, response, sheetName);

                if(result){
                    orderNumber = oms.getOrderNumber();
                    System.out.println("Mã đơn hàng được tạo là: " + orderNumber);
                    //result = oms.verifyOrder(i, orderNumber1, orderNumber2, sheetName);
                }

                reportToExcel(result, i, oms.getResultMessage());
            }
        }
    }
}
