package oms;

import apis.oms.Oms;
import object.BaseTest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_CreateOrder extends BaseTest {

    public TC_CreateOrder(){
        super.sheetName = "Create_Order";
    }

    @Parameters("refs")
    @Test
    public void TestCreate(@Optional("All") String reference) {
        Oms oms = new Oms();
        for(int i=1; i <= 1; i++){
            oms.resetOrderNumber();

            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            String expectOrderDetail = XLSWorker.getValueFromExcel(i, "ExpectOrderDetail", sheetName);
            JSONArray array = new JSONArray(expectOrderDetail);

            if(reference.equals("All") || reference.equals(refs)){
                String response = oms.run(i, "", sheetName, null);
                boolean result = oms.verifyResponse(i, response, sheetName);
                if(result){
                    String orderNumber = oms.getOrderNumber();
                    System.out.println("Mã đơn hàng được tạo là: " + orderNumber);
                    ((JSONObject) array.get(0)).put("OrderNumber", orderNumber);
                    result = oms.verifyOrder(array);
                }

                reportToExcel(result, i, oms.getResultMessage());
            }
        }
    }
}
