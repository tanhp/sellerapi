package oms;

import apis.oms.Oms;
import object.BaseTest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_UpdateOrderStatus extends BaseTest {
    public TC_UpdateOrderStatus(){
        super.sheetName = "Update_Shipment_Status";
    }

    @Parameters("refs")
    @Test
    public void TestCreate(@Optional("All") String reference) {
        Oms oms = new Oms();
        JSONObject data = new JSONObject();

        for(int i=1; i <= sheet.getLastRowNum()-3; i++){
            String orderNumber = XLSWorker.getValueFromExcel(i, "order_number", sheetName);
            String expectOrderDetail = XLSWorker.getValueFromExcel(i, "ExpectOrderDetail", sheetName);
            JSONArray array = new JSONArray(expectOrderDetail);

            if(!orderNumber.equals("GetOldOrderNumber")){

                if(orderNumber.contains("Create")){
                    oms.resetOrderNumber();

                    // Bước 1: Tạo đơn hàng
                    int row1 = XLSWorker.getRowNumByName(orderNumber, "Refs", "Create_Order");
                    oms.run(row1, "", "Create_Order", null);
                    data.put("orderNumber", oms.getOrderNumber());

                    // Bước 2: Xác nhận đơn hàng
                    int row2 = XLSWorker.getRowNumByName(orderNumber, "OrderNumber", "Confirm_Order");
                    oms.run(row2, "", "Confirm_Order", data);

                    // Bước 3: Tạo mã vận đơn
                    int row3 = XLSWorker.getRowNumByName(orderNumber, "OrderNumber", "Create_Shipment");
                    oms.run(row3, "", "Create_Shipment", data);

                }else{
                    oms.setOrderNumber(orderNumber);
                    data.put("orderNumber", orderNumber);
                }
            }else{
                data.put("orderNumber", oms.getOrderNumber());
            }



            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String response = oms.run(i, "", sheetName, data);
                boolean result = oms.verifyResponse(i, response, sheetName);

                if(result){
                    System.out.println("Mã đơn hàng được tạo là: " + oms.getOrderNumber());
                    ((JSONObject) array.get(0)).put("OrderNumber", oms.getOrderNumber());
                    result = oms.verifyOrder(array);
                }

                reportToExcel(result, i, oms.getResultMessage());
            }
        }
    }
}
