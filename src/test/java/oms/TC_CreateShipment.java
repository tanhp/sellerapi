package oms;

import apis.oms.Oms;
import object.BaseTest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_CreateShipment extends BaseTest {
    public TC_CreateShipment(){
        super.sheetName = "Create_Shipment";
    }

    @Parameters("refs")
    @Test
    public void TestCreateShipment(@Optional("All") String reference) {
        Oms oms = new Oms();
        JSONObject data = new JSONObject();

        for(int i=1; i <= sheet.getLastRowNum(); i++){
            oms.resetOrderNumber();

            String orderNumber = XLSWorker.getValueFromExcel(i, "OrderNumber", sheetName);
            String expectOrderDetail = XLSWorker.getValueFromExcel(i, "ExpectOrderDetail", sheetName);
            JSONArray array = new JSONArray(expectOrderDetail);

            if(orderNumber.contains("Create")){
                // Bước 1: Tạo đơn hàng
                int row1 = XLSWorker.getRowNumByName(orderNumber, "Refs", "Create_Order");
                oms.run(row1, "", "Create_Order", null);
                data.put("orderNumber", oms.getOrderNumber());

                // Bước 2: Xác nhận đơn hàng
                int row2 = XLSWorker.getRowNumByName(orderNumber, "OrderNumber", "Confirm_Order");
                oms.run(row2, "", "Confirm_Order", data);
            }else{
                data.put("orderNumber", orderNumber);
            }

            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                // Bước 3: Tạo mã vận đơn
                String response = oms.run(i, "", sheetName, data);
                boolean result = oms.verifyResponse(i, response, sheetName);

                if(result){
                    orderNumber = oms.getOrderNumber();
                    System.out.println("Mã đơn hàng được tạo là: " + orderNumber);
                    ((JSONObject) array.get(0)).put("OrderNumber", orderNumber);
                    result = oms.verifyOrder(array);
                }

                reportToExcel(result, i, oms.getResultMessage());
            }
        }
    }
}
