package sku;
import apis.login.Authentication;
import apis.product.Product;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.*;
import utils.Helper;
import utils.XLSWorker;

public class TC_Product_AddProduct extends BaseTest {
    public TC_Product_AddProduct(){
        super.sheetName = "AddProduct";
    }

    @Parameters("refs")
    @Test
    public void TestAddProduct(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        Product product = new Product();
        JSONObject data = new JSONObject();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            System.out.println("Refs: " + refs);
            System.out.println("Reference: " + reference);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);

                String cate2Id = XLSWorker.getValueFromExcel(i, "Cat2Id", sheetName);
                String cate3Id = XLSWorker.getValueFromExcel(i, "Cat3Id", sheetName);
                String cate4Id = XLSWorker.getValueFromExcel(i, "Cat4Id", sheetName);
                int cat2Id = product.getCategoryByRoot(cate2Id, token);
                data.put("Cat2Id", cat2Id);
                int cat3Id = product.getCategoryByParentId(cat2Id, cate3Id, token);
                data.put("Cat3Id", cat3Id);
                int cat4Id = product.getCategoryByParentId(cat3Id, cate4Id, token);
                data.put("Cat4Id", cat4Id);

                System.out.println("---Get Product Attributes---");
                String attributes = product.getProductAttributes(String.valueOf(cat4Id), token, null);
                data.put("attributes", attributes);

                String brandName = XLSWorker.getValueFromExcel(i, "BrandName", sheetName);
                if(!brandName.equals("")){
                    System.out.println("---Get Brand Id---");
                    int brandId = product.getBrandIdByName(cat4Id, brandName, token);
                    data.put("brandId", brandId);
                    System.out.println("---Get Certificate By Brand Id---");
                    data.put("certificates", product.getCertificatesByBrandId(cat4Id, brandId, token));
                }

                String response = product.run(i, token, sheetName, data);
                boolean result = product.verifyResponse(i, response, sheetName );
                if(response.contains("id") && result){
                    int productId = product.getProductId(response);
                    Helper.sleep(5000);
                    String resProductList = product.run(1, token, "ProductList", null);
                    result = product.verifyProductInList(productId, resProductList);
                    if(result){
                        product.delete(productId, token);
//                        Helper.sleep(7000);
//                        resProductList = product.run(1, token, "ProductList", null);
//                        result = product.verifyProductNotInList(productId, resProductList);
                    }
                }
                reportToExcel(result,i, refs, product.getResultMessage());
                reportToExcel2(result, i, product.jsonInput, product.jsonExpect);
            }

        }
    }

}
