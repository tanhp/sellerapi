package sku;

import apis.login.Authentication;
import apis.product.Product;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Product_CreateDraft extends BaseTest {
    public TC_Product_CreateDraft(){
        super.sheetName = "CreateDraft";
    }

    @Parameters("refs")
    @Test
    public void TestCreateDraft(@Optional("All") String reference) throws InterruptedException {
        Authentication authen = new Authentication();
        Product product = new Product();
        JSONObject data = new JSONObject();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String categoryName = XLSWorker.getValueFromExcel(i, "Category", sheetName);
                String cateResponse = product.getCattegoryByName(categoryName, token);
                data.put("cateResponse", cateResponse);
                String cate4 = product.getCate4(cateResponse);
                String attributes = product.getProductAttributes(cate4, token, null);
                data.put("attributes", attributes);

                String brandName = XLSWorker.getValueFromExcel(i, "BrandName", sheetName);
                if(!brandName.equals("")){
                    System.out.println("---Get Brand Id---");
                    int brandId = product.getBrandIdByName(Integer.valueOf(cate4), brandName, token);
                    data.put("brandId", brandId);
                    System.out.println("---Get Certificate By Brand Id---");
                    data.put("certificates", product.getCertificatesByBrandId(Integer.valueOf(cate4), brandId, token));
                }


                String response = product.run(i, token, sheetName, data);
                boolean result = product.verifyResponse(i, response, sheetName );
                Thread.sleep(5000);
                if(response.contains("id") && result){
                    int productId = product.getProductId(response);
                    String resProductList = product.run(1, token, "ProductList", null);
                    result = product.verifyProductInList(productId, resProductList);
                    if(result){
                        product.delete(productId, token);
                        Thread.sleep(5000);
                        resProductList = product.run(1, token, "ProductList", null);
                        result = product.verifyProductNotInList(productId, resProductList);
                    }
                }
                reportToExcel(result,i, refs, product.getResultMessage());
                reportToExcel2(result, i, product.jsonInput, product.jsonExpect);
            }
        }
    }
}
