package sku;

import apis.login.Authentication;
import apis.shop.Album;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Album_AddAlbum extends BaseTest {
    public TC_Album_AddAlbum(){
        super.sheetName = "AddAlbum";
    }

    @Parameters("refs")
    @Test
    public void TestAddAlbum(@Optional("All") String reference){
        Authentication authen = new Authentication();
        Album album = new Album();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String response = album.run(i, token, sheetName, null);
                boolean result = album.verifyResponse(i, response, sheetName);
                if(result){
                    int albumId = album.getAlbumId(response);
                    String resAlbumList = album.getAlbumList(token);
                    result = album.verifyAlbumIdInList(albumId, resAlbumList);
                    if(result){
                        String resAlbumDetail = album.getAlbumDetail(albumId, token);
                        result = album.verifyAlbumIdInDetail(albumId, resAlbumDetail);
                    }
                    if(result){

                        album.deleteAlbum(albumId, album.getVersionNo(), token);
                        resAlbumList = album.getAlbumList(token);
                        result = album.verifyAlbumNotInList(albumId, resAlbumList);
                    }
                }

                reportToExcel(result,i, refs, album.getResultMessage());
                reportToExcel2(result, i, album.jsonInput, album.jsonExpect);
            }
        }
    }
}
