package sku;

import apis.login.Authentication;
import apis.shop.Album;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

import java.util.ArrayList;
import java.util.List;

public class TC_Album_UploadImage extends BaseTest {
    public TC_Album_UploadImage(){
        super.sheetName = "UploadImage";
    }

    @Parameters("refs")
    @Test
    public void TestUploadImage(@Optional("All") String reference){
        Authentication authen = new Authentication();
        Album album = new Album();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);

                String fileName = XLSWorker.getValueFromExcel(i, "name", sheetName);
                String contentType = XLSWorker.getValueFromExcel(i, "type", sheetName);
                String albumId = XLSWorker.getValueFromExcel(i, "AlbumId", sheetName);
                String albumIdB = XLSWorker.getValueFromExcel(i, "albumid", sheetName);
                String boundaryName = "moxieboundary";
                List<String> contents = new ArrayList<>();
                contents.add("START");
                contents.add("Content-Disposition: form-data; name=\"name\"");
                contents.add("CRLF");
                contents.add(fileName);
                contents.add("START");
                contents.add("Content-Disposition: form-data; name=\"chunk\"");
                contents.add("CRLF");
                contents.add("0");
                contents.add("START");
                contents.add("Content-Disposition: form-data; name=\"chunks\"");
                contents.add("CRLF");
                contents.add("1");
                contents.add("START");
                contents.add("Content-Disposition: form-data; name=\"AlbumId\"");
                contents.add("CRLF");
                contents.add(albumId);
                contents.add("START");
                contents.add("Content-Disposition: form-data; name=\"file\"; filename=\"" + fileName + "\"");
                contents.add("Content-Type: " + contentType );
                contents.add("CRLF");
                contents.add("DATA");
                contents.add("CRLF");
                contents.add("END");
                String response = album.upload(i, token, sheetName, fileName, boundaryName, contents);
                boolean result = album.verifyResponse(i, response, sheetName);
                if(result){
                    int imageId = album.getImageId(response);
                    System.out.println(imageId);
                    String resAlbumDetail = album.getAlbumDetail(Integer.valueOf(albumId), token);
                    result = album.verifyImageInAlbum(imageId, resAlbumDetail);
                    if(result){
                        album.moveImage(imageId, Integer.valueOf(albumIdB), token);
                        resAlbumDetail = album.getAlbumDetail(Integer.valueOf(albumId), token);
                        boolean a = album.verifyImageNotInAlbum(imageId, resAlbumDetail);
                        resAlbumDetail = album.getAlbumDetail(Integer.valueOf(albumIdB), token);
                        boolean b = album.verifyImageInAlbum(imageId, resAlbumDetail);
                        result = (a&&b);
                    }
                    if(result){
                        album.deleteImage(imageId, Integer.valueOf(albumIdB), token);
                        resAlbumDetail = album.getAlbumDetail(Integer.valueOf(albumIdB), token);
                        result = album.verifyImageNotInAlbum(imageId, resAlbumDetail);
                    }
                }

                reportToExcel(result,i, refs, album.getResultMessage());
                reportToExcel2(result, i, album.jsonInput, album.jsonExpect);
            }

        }
    }
}
