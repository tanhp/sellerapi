package sku;

import apis.login.Authentication;
import apis.product.Product;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Mobile_Product_ChangeStock extends BaseTest {
    public TC_Mobile_Product_ChangeStock(){
        super.sheetName = "ChangeStock";
    }

    @Parameters("refs")
    @Test
    public void TC_ChangeStock(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        Product product = new Product();
        JSONObject data = new JSONObject();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String productId = XLSWorker.getValueFromExcel(i, "Id", sheetName);
                String versionNo = product.getVersionNoOnMobile(productId, token);
                data.put("versionNo", versionNo);
                String response = product.run(i, token, sheetName, data);
                boolean result = product.verifyResponse(i, response, sheetName);
                if(result){

                }
                reportToExcel(result,i, refs, product.getResultMessage());
                reportToExcel2(result, i, product.jsonInput, product.jsonExpect);
            }
        }
    }
}
