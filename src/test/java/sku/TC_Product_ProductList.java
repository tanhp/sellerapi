package sku;

import apis.login.Authentication;
import apis.product.Product;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Product_ProductList extends BaseTest {
    public TC_Product_ProductList(){
        super.sheetName = "ProductList";
    }

    @Parameters("refs")
    @Test
    public void TestProductList(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        Product product = new Product();

        for(int i=1; i <= sheet.getLastRowNum(); i++){
            System.out.println("Start i: " + i);
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String response = product.run(i, token, sheetName, null);
                boolean result = product.verifyResponse(i, response, sheetName);
                if(result){
                    result = product.verifyProductList(i, response, token);
                }
                if(result && i==1){
                    //result = product.exportProduct(i, sheetName, token);
                }
                reportToExcel(result,i, refs, product.getResultMessage());
                reportToExcel2(result, i, product.jsonInput, product.jsonExpect);
            }
            System.out.println("End i: " + i);
        }
    }
}
