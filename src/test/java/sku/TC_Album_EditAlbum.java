package sku;

import apis.login.Authentication;
import apis.shop.Album;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Album_EditAlbum extends BaseTest {
    public TC_Album_EditAlbum(){
        super.sheetName = "EditAlbum";
    }

    @Parameters("refs")
    @Test
    public void TestEditAlbum(@Optional("All") String reference){
        Authentication authen = new Authentication();
        Album album = new Album();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String response = album.run(i, token, sheetName, null);
                boolean result = album.verifyResponse(i, response, sheetName);
                reportToExcel(result,i, refs, album.getResultMessage());
                reportToExcel2(result, i, album.jsonInput, album.jsonExpect);
            }
        }
    }
}
