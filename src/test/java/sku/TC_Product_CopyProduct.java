package sku;

import apis.login.Authentication;
import apis.product.Product;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Product_CopyProduct extends BaseTest {

    public TC_Product_CopyProduct(){
        super.sheetName = "CopyProduct";
    }

    @Parameters("refs")
    @Test
    public void TestCopyProduct(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        Product product = new Product();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String response = product.run(i, token, sheetName, null);
                boolean result = product.verifyResponse(i, response, sheetName);
                if(response.contains("id") && result){
                    String cloneProductId = product.getCloneProductId(response);
                    String resCopyProduct = product.copyProduct(cloneProductId, token);
                    result = product.verifyCopiedProduct(cloneProductId, new JSONObject(resCopyProduct));
                }
                reportToExcel(result,i, refs, product.getResultMessage());
                reportToExcel2(result, i, product.jsonInput, product.jsonExpect);
            }
        }
    }
}
