package sku;

import apis.login.Authentication;
import apis.product.Product;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Product_EditProduct extends BaseTest {

    public TC_Product_EditProduct(){
        super.sheetName = "EditProduct";
    }

    @Parameters("refs")
    @Test
    public void C001_EditProduct(@Optional("All") String reference) throws InterruptedException {
        Authentication authen = new Authentication();
        Product product = new Product();
        JSONObject data = new JSONObject();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String categoryName = XLSWorker.getValueFromExcel(i, "Category", sheetName);
                String cateResponse = product.getCattegoryByName(categoryName, token);
                data.put("cateResponse", cateResponse);

                String cate4 = product.getCate4(cateResponse);
                String attributes = product.getProductAttributes(cate4, token, null);
                data.put("attributes", attributes);

                String productId = XLSWorker.getValueFromExcel(i, "Id",sheetName);

                String currentImage = product.getCurrentImage(productId, token);
                data.put("productImage", currentImage);

                String versionNo = product.getVersionNo(productId, token);
                data.put("versionNo", versionNo);

                String response = product.run(i, token, sheetName, data);
                boolean result = product.verifyResponse(i, response, sheetName );
                reportToExcel(result,i, refs, product.getResultMessage());
                reportToExcel2(result, i, product.jsonInput, product.jsonExpect);
                Thread.sleep(1000);
            }

        }
    }

}
