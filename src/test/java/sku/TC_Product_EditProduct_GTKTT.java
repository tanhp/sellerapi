package sku;

import apis.login.Authentication;
import apis.product.Product;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Product_EditProduct_GTKTT extends BaseTest {

    public TC_Product_EditProduct_GTKTT(){
        super.sheetName = "GTKTT_EditProduct";
    }

    @Parameters("refs")
    @Test
    public void C001_EditProduct_GTKTT(@Optional("All") String reference) throws InterruptedException {
        Authentication authen = new Authentication();
        Product product = new Product();
        JSONObject data = new JSONObject();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String categoryName = XLSWorker.getValueFromExcel(i, "Category", sheetName);
                String cateResponse = product.getCattegoryByName(categoryName, token);
                data.put("cateResponse", cateResponse);
                String productId = XLSWorker.getValueFromExcel(i, "Id",sheetName);

                String cate4 = product.getCate4(cateResponse);
                JSONObject currentVariants = product.getCurrentVariants(productId, token);
                String attributes = product.getProductAttributes(cate4, token, product.getAttribute3(i, sheetName, currentVariants));
                data.put("attributes", attributes);
                data.put("variants", product.getVariant(i, sheetName, currentVariants));


                String currentImage = product.getCurrentImage(productId, token);
                data.put("productImage", currentImage);

                String versionNo = product.getVersionNo(productId, token);
                data.put("versionNo", versionNo);
                String response = product.run(i, token, sheetName, data);
                boolean result = product.verifyResponse(i, response, sheetName );
                reportToExcel(result,i, refs, product.getResultMessage());
                reportToExcel2(result, i, product.jsonInput, product.jsonExpect);
                Thread.sleep(1000);
            }
        }
    }

}
