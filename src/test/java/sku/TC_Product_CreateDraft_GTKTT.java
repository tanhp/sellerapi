package sku;

import apis.login.Authentication;
import apis.product.Product;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Product_CreateDraft_GTKTT extends BaseTest {
    public TC_Product_CreateDraft_GTKTT(){
        super.sheetName = "GTKTT_CreateDraft";
    }

    @Parameters("refs")
    @Test
    public void TestCreateDraftGTKTT(@Optional("All") String reference) throws InterruptedException {
        Authentication authen = new Authentication();
        Product product = new Product();
        JSONObject data = new JSONObject();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String categoryName = XLSWorker.getValueFromExcel(i, "Category", sheetName);
                String cateResponse = product.getCattegoryByName(categoryName, token);
                data.put("cateResponse", cateResponse);

                String cate4 = product.getCate4(cateResponse);
                String attributes = product.getProductAttributes(cate4, token, product.getAttribute3(i, sheetName, null));
                data.put("attributes", attributes);

                data.put("variants", product.getVariant(i, sheetName, null));
                String response = product.run(i, token, sheetName, data);
                boolean result = product.verifyResponse(i, response, sheetName );
                Thread.sleep(5000);
                if(response.contains("id") && result){
                    int productId = product.getProductId(response);
                    String resProductList = product.run(1, token, "ProductList", null);
                    result = product.verifyProductInList(productId, resProductList);
                    if(result){
                        product.delete(productId, token);
                        Thread.sleep(5000);
                        resProductList = product.run(1, token, "ProductList", null);
                        result = product.verifyProductNotInList(productId, resProductList);
                    }
                }
                reportToExcel(result,i, refs, product.getResultMessage());
                reportToExcel2(result, i, product.jsonInput, product.jsonExpect);
            }
        }
    }
}
