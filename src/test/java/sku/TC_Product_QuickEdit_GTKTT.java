package sku;

import apis.login.Authentication;
import apis.product.Product;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Product_QuickEdit_GTKTT extends BaseTest {

    public TC_Product_QuickEdit_GTKTT(){
        super.sheetName = "GTKTT_QuickEdit";
    }

    @Parameters("refs")
    @Test
    public void C001_QuickEdit(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        Product product = new Product();
        JSONObject data = new JSONObject();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String productId = XLSWorker.getValueFromExcel(i, "productId", sheetName);
                JSONObject currentVariants = product.getCurrentVariants(productId, token);
                data.put("variants", product.getVariant(i, sheetName, currentVariants));
                String versionNo = product.getVersionNo(productId, token);
                data.put("versionNo", versionNo);

                String response = product.run(i, token, sheetName, data);
                boolean result = product.verifyResponse(i, response, sheetName);
                reportToExcel(result,i, refs, product.getResultMessage());
                reportToExcel2(result, i, product.jsonInput, product.jsonExpect);
            }

        }
    }
}
