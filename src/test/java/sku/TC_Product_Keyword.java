package sku;

import apis.login.Authentication;
import apis.product.Product;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Product_Keyword extends BaseTest {
    public TC_Product_Keyword(){
        super.sheetName = "Keyword";
    }

    @Parameters("refs")
    @Test
    public void TestListActive(@Optional("All") String reference) {
        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                Authentication authen = new Authentication();
                Product product = new Product();

                String token = authen.getToken(i, sheetName);
                String response = product.run(i, token, sheetName, null);
                boolean result = product.verifyResponse(i, response, sheetName);
                reportToExcel(result,i, refs, product.getResultMessage());
                reportToExcel2(result, i, product.jsonInput, product.jsonExpect);
            }
        }
    }
}
