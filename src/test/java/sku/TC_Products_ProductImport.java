package sku;

import apis.login.Authentication;
import apis.product.Product;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class TC_Products_ProductImport extends BaseTest {

    public TC_Products_ProductImport(){
        super.sheetName = "ProductImport";
    }

    @Parameters("refs")
    @Test
    public void TestProductImport(@Optional("All") String reference){
        Authentication authen = new Authentication();
        Product product = new Product();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String fileName = XLSWorker.getValueFromExcel(i, "FileName", sheetName);
                String contentType = XLSWorker.getValueFromExcel(i, "ContentType", sheetName);
                int status = Integer.valueOf(XLSWorker.getValueFromExcel(i, "ExpectedStatus", sheetName));
                int type = Integer.valueOf(XLSWorker.getValueFromExcel(i, "type", sheetName));
                String boundaryName = "WebKitFormBoundary";
                List<String> contents = new ArrayList<>();
                contents.add("START");
                contents.add("Content-Disposition: form-data; name=\"file\"; filename=\"" + fileName + "\"");
                contents.add("Content-Type: " + contentType );
                contents.add("CRLF");
                contents.add("DATA");
                contents.add("CRLF");
                contents.add("START");
                contents.add("Content-Disposition: form-data; name=\"Type\"");
                contents.add("CRLF");
                contents.add(String.valueOf(type));
                contents.add("END");

                String response =  product.upload(i, token, sheetName, fileName, boundaryName, contents);
                System.out.println(response);
                boolean result = product.verifyResponse(i, response, sheetName);
                if(result){
                    int importId = product.getImportId(response);
                    result = product.checkImportStatus(importId, status, fileName, type, token);
                    if(result){
                        String resProductImport = product.getProductImports(token);
                        result = product.verifyImportHistory(importId, status, fileName, type, resProductImport);
                    }
                    if(status == 2){
                        if(result){
                            String resProductImportDetail = product.getProductImportDetail(importId, token);
                            result = product.verifyProductImportDetail(importId, resProductImportDetail);
                        }
                        if(result){
                            result = product.cancelImport(importId, token);
                        }
                    }
                }
                reportToExcel(result,i, refs, product.getResultMessage());
                reportToExcel2(result, i, product.jsonInput, product.jsonExpect);
            }
        }
    }
}
