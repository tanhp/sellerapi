package sku;

import apis.login.Authentication;
import apis.product.Product;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Products_RequestExport extends BaseTest {

    public TC_Products_RequestExport(){
        super.sheetName = "RequestExport";
    }

    @Parameters("refs")
    @Test
    public void TestRequestExport(@Optional("All") String reference){
        Authentication authen = new Authentication();
        Product product = new Product();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                int type = Integer.valueOf(XLSWorker.getValueFromExcel(i, "type", sheetName));
                int status = Integer.valueOf(XLSWorker.getValueFromExcel(i, "ExpectedStatus", sheetName));
                String response = product.run(i, token, sheetName, null);
                boolean result = product.verifyResponse(i, response, sheetName);
                if(result){
                    int exportId = product.getExportId(response);
                    result = product.checkExportStatus(exportId, status, type, token);
                    if(result){
                        String resProductExport = product.getProductExport(token);
                        result = product.verifyExportHistory(exportId,status,type,resProductExport);
                    }
                }
                reportToExcel(result,i, refs, product.getResultMessage());
                reportToExcel2(result, i, product.jsonInput, product.jsonExpect);
            }
        }
    }
}
