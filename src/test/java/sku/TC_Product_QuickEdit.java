package sku;

import apis.login.Authentication;
import apis.product.Product;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Product_QuickEdit extends BaseTest {

    public TC_Product_QuickEdit(){
        super.sheetName = "QuickEdit";
    }

    @Parameters("refs")
    @Test
    public void C001_QuickEdit(@Optional("All") String reference) {
        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                Authentication authen = new Authentication();
                Product product = new Product();
                JSONObject data = new JSONObject();

                String token = authen.getToken(i, sheetName);
                String productId = XLSWorker.getValueFromExcel(i, "Id", sheetName);
                String versionNo = product.getVersionNo(productId, token);
                data.put("versionNo", versionNo);
                String response = product.run(i, token, sheetName, data);
                boolean result = product.verifyResponse(i, response, sheetName);
                reportToExcel(result,i, refs, product.getResultMessage());
                reportToExcel2(result, i, product.jsonInput, product.jsonExpect);
            }
        }
    }

}
