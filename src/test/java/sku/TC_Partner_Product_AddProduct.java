package sku;

import apis.login.Authentication;
import apis.product.Product;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class  TC_Partner_Product_AddProduct extends BaseTest {
    public TC_Partner_Product_AddProduct(){
        super.sheetName = "AddProduct";
    }

    @Parameters("refs")
    @Test
    public void TestAddProduct(@Optional("All") String reference) throws InterruptedException {
        Authentication authen = new Authentication();
        Product product = new Product();
        JSONObject data = new JSONObject();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            System.out.println("Refs: " + refs);
            System.out.println("Reference: " + reference);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String bearerToken = authen.getBearerToken(token);

                String cate2Name = XLSWorker.getValueFromExcel(i, "Cat2Id", sheetName);
                String cate3Name = XLSWorker.getValueFromExcel(i, "Cat3Id", sheetName);
                String cate4Name = XLSWorker.getValueFromExcel(i, "Cat4Id", sheetName);
                int cat2Id = product.getCategoryByRootOnPartner(cate2Name, bearerToken);
                data.put("Cat2Id", cat2Id);
                int cat3Id = product.getCategoryByParentIdOnPartner(cat2Id, cate3Name, bearerToken);
                data.put("Cat3Id", cat3Id);
                int cat4Id = product.getCategoryByParentIdOnPartner(cat3Id, cate4Name, bearerToken);
                data.put("Cat4Id", cat4Id);

                System.out.println("---Get Product Attributes---");
                String attributes = product.getProductAttributesOnPartner(String.valueOf(cat4Id), bearerToken, null);
                data.put("attributes", attributes);

                String brandName = XLSWorker.getValueFromExcel(i, "BrandName", sheetName);
                if(!brandName.equals("")){

                }

                String response = product.run(i, bearerToken, sheetName, data);
                boolean result = product.verifyResponse(i, response, sheetName );
                if(response.contains("result") && result){
                    int productId = product.getProductId("result", response);
                    Thread.sleep(5000);
                    String resProductList = product.run(1, bearerToken, "ProductList", null);
                    result = product.verifyProductInList(productId, resProductList);

                }
                reportToExcel(result,i, refs, product.getResultMessage());
                reportToExcel2(result, i, product.jsonInput, product.jsonExpect);
            }

        }
    }
}
