package sku;

import apis.login.Authentication;
import apis.product.Product;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

import java.util.ArrayList;
import java.util.List;

public class TC_Products_ProductApplyImport extends BaseTest {

    public TC_Products_ProductApplyImport(){
        super.sheetName = "ProductApplyImport";
    }

    @Parameters("refs")
    @Test
    public void TestProductApplyImport(@Optional("All") String reference) throws InterruptedException {
        Authentication authen = new Authentication();
        Product product = new Product();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String fileName = XLSWorker.getValueFromExcel(i, "FileName", sheetName);
                String contentType = XLSWorker.getValueFromExcel(i, "ContentType", sheetName);
                int status = Integer.valueOf(XLSWorker.getValueFromExcel(i, "ExpectedStatus", sheetName));
                int type = Integer.valueOf(XLSWorker.getValueFromExcel(i, "type", sheetName));
                String boundaryName = "WebKitFormBoundary";
                List<String> contents = new ArrayList<>();
                contents.add("START");
                contents.add("Content-Disposition: form-data; name=\"file\"; filename=\"" + fileName + "\"");
                contents.add("Content-Type: " + contentType );
                contents.add("CRLF");
                contents.add("DATA");
                contents.add("CRLF");
                contents.add("START");
                contents.add("Content-Disposition: form-data; name=\"Type\"");
                contents.add("CRLF");
                contents.add(String.valueOf(type));
                contents.add("END");

                String response =  product.upload(i, token, sheetName, fileName, boundaryName, contents);
                System.out.println(response);
                boolean result = product.verifyResponse(i, response, sheetName);
                if(result){
                    int importId = product.getImportId(response);
                    result = product.checkImportStatus(importId, status, fileName, type, token);
                    if(result && status==2){
                        result = product.applyImport(importId, token);
                    }
                }
                reportToExcel(result,i, refs, product.getResultMessage());
                Thread.sleep(10000);
            }

        }
    }
}
