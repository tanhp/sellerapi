package sku;

import apis.login.Authentication;
import apis.product.Product;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Mobile_Product_AddProduct extends BaseTest {
    public TC_Mobile_Product_AddProduct(){
        super.sheetName = "AddProduct";
    }

    @Parameters("refs")
    @Test
    public void TestAddProduct(@Optional("All") String reference) throws InterruptedException {
        Authentication authen = new Authentication();
        Product product = new Product();
        JSONObject data = new JSONObject();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            System.out.println("Refs: " + refs);
            System.out.println("Reference: " + reference);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);

                String cate2Id = XLSWorker.getValueFromExcel(i, "Cat2Id", sheetName);
                String cate3Id = XLSWorker.getValueFromExcel(i, "Cat3Id", sheetName);
                String cate4Id = XLSWorker.getValueFromExcel(i, "Cat4Id", sheetName);
                int cat2Id = product.getCategoryByRootOnMobile(cate2Id, token);
                data.put("Cat2Id", cat2Id);
                int cat3Id = product.getCategoryByParentIdOnMobile(cat2Id, cate3Id, token);
                data.put("Cat3Id", cat3Id);
                int cat4Id = product.getCategoryByParentIdOnMobile(cat3Id, cate4Id, token);
                data.put("Cat4Id", cat4Id);

                System.out.println("---Get Product Attributes---");
                String attributes = product.getProductAttributesOnMobile(String.valueOf(cat4Id), token, null);
                data.put("attributes", attributes);

                String brandName = XLSWorker.getValueFromExcel(i, "BrandName", sheetName);
                if(!brandName.equals("")){
                    System.out.println("---Get Brand Id---");
                    int brandId = product.getBrandIdByNameOnMobile(cat4Id, brandName, token);
                    data.put("brandId", brandId);
                    System.out.println("---Get Certificate By Brand Id---");
                    data.put("certificates", product.getCertificatesByBrandIdOnMobile(cat4Id, brandId, token));
                }

                String response = product.run(i, token, sheetName, data);
                boolean result = product.verifyResponse(i, response, sheetName );
                if(response.contains("result") && result){
                    int productId = product.getProductId("result", response);
                    Thread.sleep(5000);
                    String resProductList = product.run(1, token, "ProductList", null);
                    result = product.verifyProductInList(productId, resProductList);
                    if(result){
                        product.deleteOnMobile(productId, token);
                        Thread.sleep(5000);
                        resProductList = product.run(1, token, "ProductList", null);
                        result = product.verifyProductNotInList(productId, resProductList);
                    }
                }
                reportToExcel(result,i, refs, product.getResultMessage());
                reportToExcel2(result, i, product.jsonInput, product.jsonExpect);
            }

        }
    }
}
