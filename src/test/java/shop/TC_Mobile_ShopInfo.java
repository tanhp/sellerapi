package shop;

import apis.login.Authentication;
import apis.shop.ShopInfo;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Mobile_ShopInfo extends BaseTest {
    public TC_Mobile_ShopInfo(){
        super.sheetName = "ShopInfo";
    }

    @Parameters("refs")
    @Test
    public void TestSaveShopInfo(@Optional("All") String reference){
        Authentication authen = new Authentication();
        ShopInfo shopInfo = new ShopInfo();
        JSONObject data = new JSONObject();

        for(int i=1; i<=1; i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String response = shopInfo.getShopInfoOnMobile(token);
                boolean result = shopInfo.verifyShopInfo(response);
                if(result){
                    String versionNo = shopInfo.getVersionNo("VersionNo", response);
                    data.put("versionNo", versionNo);

                    String shopId = shopInfo.getShopId("Id",response);
                    data.put("shopId", shopId);

                    String shopRegionName = XLSWorker.getValueFromExcel(i, "ShopRegionId", sheetName);
                    String shopDistrictName = XLSWorker.getValueFromExcel(i, "ShopDistrictId", sheetName);
                    String shopWardName = XLSWorker.getValueFromExcel(i, "ShopWardId", sheetName);
                    int shopRegionId = shopInfo.getRegionIdByName(shopRegionName, token);
                    data.put("shopRegionId", shopRegionId);
                    int shopDistrictId = shopInfo.getDictrictIdByName(shopRegionId, shopDistrictName, token);
                    data.put("shopDistrictId", shopDistrictId);
                    int shopWardId = shopInfo.getWardIdByName(shopDistrictId, shopWardName, token);
                    data.put("shopWardId", shopWardId);

                    String warehouseRegionName = XLSWorker.getValueFromExcel(i, "WarehouseRegionId", sheetName);
                    String warehouseDistrictName = XLSWorker.getValueFromExcel(i, "WarehouseDistrictId", sheetName);
                    String warehouseWardName = XLSWorker.getValueFromExcel(i, "WarehouseWardId", sheetName);
                    int warehouseRegionId = shopInfo.getRegionIdByName(warehouseRegionName, token);
                    data.put("warehouseRegionId", warehouseRegionId);
                    int warehouseDistrictId = shopInfo.getDictrictIdByName(warehouseRegionId, warehouseDistrictName, token);
                    data.put("warehouseDistrictId", warehouseDistrictId);
                    int warehouseWardId = shopInfo.getWardIdByName(warehouseDistrictId, warehouseWardName, token);
                    data.put("warehouseWardId", warehouseWardId);
                    System.out.println("get warehouseWardId: " +  data.get("warehouseWardId"));

                    String resSaveShopInfo = shopInfo.run(i,token,sheetName,data);
                    result = shopInfo.verifyResponse(i, resSaveShopInfo,sheetName);
                    reportToExcel(result,i, refs, shopInfo.getResultMessage());
                    reportToExcel2(result, i, shopInfo.jsonInput, shopInfo.jsonExpect);
                }
            }
        }
    }
}
