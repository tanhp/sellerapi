package shop;
import apis.login.Authentication;
import apis.shop.Register;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Register_Step02 extends BaseTest {
    public TC_Register_Step02(){
        super.sheetName = "Register_Step2";
    }

    @Parameters("refs")
    @Test
    public void TestCreateShop(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        Register shop = new Register();
        for(int i=1; i <=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String response = shop.run(i, token, sheetName, null);
                boolean result = shop.verifyResponse(i, response, sheetName);
                reportToExcel(result,i, refs, shop.getResultMessage());
                reportToExcel2(result, i, shop.jsonInput, shop.jsonExpect);
            }
        }
    }
}
