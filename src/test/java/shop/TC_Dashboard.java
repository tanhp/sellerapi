package shop;

import apis.login.Authentication;
import apis.shop.Dashboard;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Dashboard extends BaseTest {
    public TC_Dashboard(){
        super.sheetName = "Dashboard";
    }

    @Parameters("refs")
    @Test
    public void TestDashboard(@Optional("All") String reference){
        Authentication authen = new Authentication();
        Dashboard dashboard = new Dashboard();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String response = dashboard.run(i, token, sheetName, null);
                Boolean result = dashboard.verifyResponse(i, response, sheetName);
                reportToExcel(result,i, refs, dashboard.getResultMessage());
                reportToExcel2(result, i, dashboard.jsonInput, dashboard.jsonExpect);
            }
        }
    }
}
