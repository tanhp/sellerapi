package shop;

import apis.login.Authentication;
import apis.shop.Voucher;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Voucher_EditVoucher extends BaseTest {
    public TC_Voucher_EditVoucher(){
        super.sheetName = "EditVoucher";
    }

    @Parameters("refs")
    @Test
    public void TestVoucherList(@Optional("All") String reference){
        Authentication authen = new Authentication();
        Voucher voucher = new Voucher();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String response = voucher.run(i, token, sheetName, null);
                boolean result = voucher.verifyResponse(i, response, sheetName);
                reportToExcel(result, i, refs, voucher.getResultMessage());
                reportToExcel2(result, i, authen.jsonInput, authen.jsonExpect);
            }
        }
    }
}
