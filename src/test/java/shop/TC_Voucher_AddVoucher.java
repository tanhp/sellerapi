package shop;

import apis.login.Authentication;
import apis.shop.Voucher;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Voucher_AddVoucher extends BaseTest {
    public TC_Voucher_AddVoucher(){
        super.sheetName = "AddVoucher";
    }

    @Parameters("refs")
    @Test
    public void TestVoucherList(@Optional("All") String reference){
        Authentication authen = new Authentication();
        Voucher voucher = new Voucher();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String response = voucher.run(i, token, sheetName, null);
                boolean result = voucher.verifyResponse(i, response, sheetName);
                if(result && response.equals("{\"IsError\":false}")){
                    String resVoucherList = voucher.run(1, token,"VoucherList",null);
                    result = voucher.verifyVoucherInList(voucher.getVoucherCode(), resVoucherList);
                    if(result){
                        String resVoucherDetail = voucher.getVoucherDetail(voucher.getVoucherId(), token);
                        result = voucher.verifyVoucherInDetail(voucher.getVoucherCode(), resVoucherDetail);
                    }
                    if(result){
                        voucher.cancelVoucher(voucher.getVoucherId(), voucher.getVersionNo(), token);
                        resVoucherList = voucher.run(1, token,"VoucherList",null);
                        //result = voucher.verifyVoucherInList(voucher.getVoucherCode(), resVoucherList);
                    }
                }
                reportToExcel(result, i, refs, voucher.getResultMessage());
                reportToExcel2(result, i, authen.jsonInput, authen.jsonExpect);
            }
        }
    }
}
