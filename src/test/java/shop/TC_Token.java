package shop;

import apis.login.Authentication;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.JSON;
import utils.XLSWorker;

public class TC_Token extends BaseTest {
    public TC_Token(){
        super.sheetName = "Token";
    }

    @Parameters("refs")
    @Test
    public void TestToken(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String resLogin = authen.login(i, sheetName);
                String token = new JSONObject(resLogin).getJSONObject("result").getString("token");
                String response = authen.run(i, token, sheetName, null);
                boolean result = authen.verifyResponse(i, response, sheetName);
                if(result){
                    String endPoint = XLSWorker.getValueFromExcel(i, "EndPoint", sheetName);
                    switch (endPoint){
                        case "ENDPOINT_API_TOKEN_USERCONTEXT":
                            result = authen.verifyUserContext(new JSONObject(resLogin).getJSONObject("result").getJSONObject("userContextDto"), new JSONObject(response).getJSONObject("result"));
                            break;
                        case "ENDPOINT_API_TOKEN_ROLES":
                        case "ENDPOINT_API_WEB_STORE_GETROLES":
                            result = authen.verifyRoles(new JSONObject(resLogin).getJSONObject("result").getJSONObject("userContextDto").getJSONArray("roles"), new JSONObject(response).getJSONArray("result"));
                            break;
                        case "ENDPOINT_API_TOKEN_SENDOID":
                            result = authen.verifySendoId(new JSONObject(resLogin).getJSONObject("result").getJSONObject("userContextDto").getString("userName"), new JSONObject(response).getString("result"));
                            break;
                        case "ENDPOINT_API_TOKEN_WEB_MENU":
                            result = authen.verifyMenu(new JSONObject(resLogin).getJSONObject("result").getJSONObject("userContextDto").getJSONArray("menus"), new JSONObject(response).getJSONArray("result"));
                            result = true;
                            break;
                        case "ENDPOINT_API_WEB_STORE_STOREACCESS":
                            result = authen.verifyStoreAccess(new JSONObject(resLogin).getJSONObject("result").getJSONObject("userContextDto").getJSONArray("storeAccess"), new JSONObject(response).getJSONArray("result"));
                            break;
                    }
                }
                reportToExcel(result, i, refs, authen.getResultMessage());
                reportToExcel2(result, i, authen.jsonInput, authen.jsonExpect);
            }
        }
    }
}
