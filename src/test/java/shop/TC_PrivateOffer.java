package shop;

import apis.login.Authentication;
import apis.product.Product;
import apis.shop.PrivateOffer;
import object.BaseTest;
import object.Buyer;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_PrivateOffer extends BaseTest {
    public TC_PrivateOffer(){
        super.sheetName = "PrivateOffer";
    }

    @Parameters("refs")
    @Test
    public void TestPrivateOffer(@Optional("All") String reference) {
        PrivateOffer po = new PrivateOffer();
        Authentication authen = new Authentication();
        Product product = new Product();
        Buyer buyer = new Buyer();
        JSONObject data = new JSONObject();

        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){

                // 1. Login
                String token = authen.getToken(i, sheetName);

                // 2. Create new product
                String cate2Id = XLSWorker.getValueFromExcel(1, "Cat2Id", "AddProduct");
                String cate3Id = XLSWorker.getValueFromExcel(1, "Cat3Id", "AddProduct");
                String cate4Id = XLSWorker.getValueFromExcel(1, "Cat4Id", "AddProduct");
                int cat2Id = product.getCategoryByRoot(cate2Id, token);
                data.put("Cat2Id", cat2Id);
                int cat3Id = product.getCategoryByParentId(cat2Id, cate3Id, token);
                data.put("Cat3Id", cat3Id);
                int cat4Id = product.getCategoryByParentId(cat3Id, cate4Id, token);
                data.put("Cat4Id", cat4Id);

                System.out.println("---Get Product Attributes---");
                String attributes = product.getProductAttributes(String.valueOf(cat4Id), token, null);
                data.put("attributes", attributes);

                String brandName = XLSWorker.getValueFromExcel(1, "BrandName", "AddProduct");
                if(!brandName.equals("")){
                    System.out.println("---Get Brand Id---");
                    int brandId = product.getBrandIdByName(cat4Id, brandName, token);
                    data.put("brandId", brandId);
                    System.out.println("---Get Certificate By Brand Id---");
                    data.put("certificates", product.getCertificatesByBrandId(cat4Id, brandId, token));
                }

                String resProduct = product.run(1, token, "AddProduct", data);
                int productId = product.getProductId(resProduct);

                // 3. Like product
                buyer.likeProduct(productId);

                // 4. Private offer
                String resGetListingGroup = po.getListPagingGroupPrivateOfferWithProduct(token);
                System.out.println("A");
                boolean result = po.verifyListPagingGroupPrivateOfferWithProduct(Integer.valueOf(buyer.getUserId()), new JSONObject(resGetListingGroup));
                System.out.println("B");
                if(result){
                    int poId = po.getPrivateOfferId(Integer.valueOf(buyer.getUserId()), new JSONObject(resGetListingGroup));
                    System.out.println("E");
                    data.put("privateOfferId", poId);
                    String resGetListing = po.getListPagingPrivateOfferWithProduct(Integer.valueOf(buyer.getUserId()), token);
                    System.out.println("C");
                    result = po.verifyListPagingPrivateOfferWithProduct(poId, new JSONObject(resGetListing));
                    System.out.println("D");
                    if(result){
                        String resGetDetail = po.getPrivateOfferDetailById(poId, token);
                        result = po.verifyPrivateOfferDetailById(poId, new JSONObject(resGetDetail));
                        if(result){
                            String type = XLSWorker.getValueFromExcel(i, "TypeDiscount", sheetName);
                            if(type.equals("1")){
                                String percent = XLSWorker.getValueFromExcel(i, "VoucherPercentValue", sheetName);
                                int price = new JSONObject(resGetDetail).getJSONObject("Data").getInt("TotalMinPrice");
                                int value = price*Integer.valueOf(percent)/100;
                                data.put("voucherValue", value);
                                System.out.println("Value: " + value);
                            }
                            String response = po.run(i, token, sheetName, data);
                            result = po.verifyResponse(i, response, sheetName);
                        }
                    }
                }

                reportToExcel(result,i, refs, po.getResultMessage());
                reportToExcel2(result, i, po.jsonInput, po.jsonExpect);

                product.delete(productId, token);
            }
        }
    }
}
