package shop;

import apis.login.Authentication;
import apis.shop.Carrier;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_DropOff extends BaseTest {
    public TC_DropOff(){
        super.sheetName = "DropOff";
    }

    @Parameters("refs")
    @Test
    public void TestDropOff(@Optional("All") String reference){
        Authentication authen = new Authentication();
        Carrier carrier = new Carrier();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String resCarrierList = carrier.getDropOffList(token);
                boolean result = carrier.verifyDropOffList(resCarrierList);
                if(result){
                    String response = carrier.run(i, token, sheetName, null);
                    result = carrier.verifyResponse(i, response, sheetName);
                    reportToExcel(result, i, refs, carrier.getResultMessage());
                    reportToExcel2(result, i, carrier.jsonInput, carrier.jsonExpect);
                }
            }
        }
    }
}
