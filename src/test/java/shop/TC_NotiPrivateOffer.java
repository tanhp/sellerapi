package shop;

import apis.login.Authentication;
import apis.shop.Shop;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_NotiPrivateOffer extends BaseTest {
    public TC_NotiPrivateOffer(){
        super.sheetName = "NotiPrivateOffer";
    }

    @Parameters("refs")
    @Test
    public void TestNotiPrivateOffer(@Optional("All") String reference){
        Authentication authen = new Authentication();
        Shop shop = new Shop();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String resGetSettingPO = shop.getStoreSettingsNotiPrivateOffer(token);
                boolean result = shop.verifySettingPrivateOffer(new JSONObject(resGetSettingPO));
                if(result){
                    String response = shop.run(i, token, sheetName, null);
                    result = shop.verifyResponse(i, response, sheetName);
                }
                reportToExcel(result,i, refs, shop.getResultMessage());
                reportToExcel2(result, i, shop.jsonInput, shop.jsonExpect);
            }
        }
    }
}
