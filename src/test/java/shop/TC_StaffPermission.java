package shop;

import apis.login.Authentication;
import apis.shop.Shop;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_StaffPermission extends BaseTest {
    public TC_StaffPermission(){
        super.sheetName = "Permission";
    }

    @Parameters("refs")
    @Test
    public void TestPermission(@Optional("All") String reference){
        Authentication authen = new Authentication();
        Shop shop = new Shop();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                shop.validateStoreFunction(token);
                shop.getStaffPermission("tester01gm@gmail.com", token);
                shop.validateUser("19901309", token);
                String response = shop.run(i, token, sheetName, null);
                boolean result = shop.verifyResponse(i, response, sheetName);
                shop.validateStoreFunction(token);
                shop.getStaff(token);
                reportToExcel(result,i, refs, shop.getResultMessage());
                reportToExcel2(result, i, shop.jsonInput, shop.jsonExpect);
            }
        }
    }
}
