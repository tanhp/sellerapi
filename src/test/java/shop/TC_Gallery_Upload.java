package shop;

import apis.login.Authentication;
import apis.shop.Gallery;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Gallery_Upload extends BaseTest {
    public TC_Gallery_Upload(){
        super.sheetName = "Upload";
    }

    @Parameters("refs")
    @Test
    public void TestUpload(@Optional("All") String reference){
        Authentication authen = new Authentication();
        Gallery gallery = new Gallery();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String boundary = XLSWorker.getValueFromExcel(i, "boundary", sheetName);
                String name = XLSWorker.getValueFromExcel(i, "name", sheetName);
                String type = XLSWorker.getValueFromExcel(i, "type", sheetName);
                String chunk = XLSWorker.getValueFromExcel(i, "chunk", sheetName);
                String chunks = XLSWorker.getValueFromExcel(i, "chunks", sheetName);
                String albumid = XLSWorker.getValueFromExcel(i, "albumid", sheetName);

                String response = gallery.upload(i, token, sheetName, name, boundary, gallery.getFormData(name, type, chunk, chunks, albumid));
                boolean result = gallery.verifyResponse(i, response, sheetName);
                reportToExcel(result, i, refs, gallery.getResultMessage());
                reportToExcel2(result, i, gallery.jsonInput, gallery.jsonExpect);
            }
        }
    }
}
