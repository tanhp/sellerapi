package shop;

import apis.login.Authentication;
import apis.shop.Carrier;
import apis.shop.Voucher;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Carrier extends BaseTest {
    public TC_Carrier(){
        super.sheetName = "Carrier";
    }

    @Parameters("refs")
    @Test
    public void TestCarrier(@Optional("All") String reference){
        Authentication authen = new Authentication();
        Carrier carrier = new Carrier();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String resCarrierList = carrier.getCarrierList(token);
                boolean result = carrier.verifyCarrierList(resCarrierList);
                if(result){
                    String response = carrier.run(i, token, sheetName, null);
                    result = carrier.verifyResponse(i, response, sheetName);
                    reportToExcel(result,i, refs, carrier.getResultMessage());
                    reportToExcel2(result, i, carrier.jsonInput, carrier.jsonExpect);
                }
            }
        }
    }
}
