package shop;

import apis.login.Authentication;
import apis.shop.Customer;
import apis.shop.Dashboard;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Customer extends BaseTest {
    public TC_Customer(){
        super.sheetName = "Customer";
    }

    @Parameters("refs")
    @Test
    public void TestDashboard(@Optional("All") String reference){
        Authentication authen = new Authentication();
        Customer customer = new Customer();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String response = customer.run(i, token, sheetName, null);
                Boolean result = customer.verifyResponse(i, response, sheetName);
                reportToExcel(result,i, refs, customer.getResultMessage());
                reportToExcel2(result, i, customer.jsonInput, customer.jsonExpect);
            }
        }
    }
}
