package shop;

import apis.login.Authentication;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;


public class TC_Login extends BaseTest {

    public TC_Login(){
        super.sheetName = "Login";
    }

    @Parameters("refs")
    @Test
    public void TestLogin(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String response = authen.run(i, "", sheetName, null);
                boolean result = authen.verifyResponse(i, response, sheetName);
                reportToExcel(result, i, refs, authen.getResultMessage());
                reportToExcel2(result, i, authen.jsonInput, authen.jsonExpect);
            }
        }
    }
}


