package shop;

import apis.login.Authentication;
import apis.shop.News;
import apis.shop.NewsPublic;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_News extends BaseTest {
    public TC_News(){
        super.sheetName = "News";
    }

    @Parameters("refs")
    @Test
    public void TestNews(@Optional("All") String reference){
        Authentication authen = new Authentication();
        News news = new News();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String response = news.run(i, token,sheetName, new JSONObject());
                boolean result = news.verifyResponse(i, response, sheetName);
                if(result && response.contains("title")){
                    JSONObject firstNews = news.getFirstNews(response);
                    int id = (int) firstNews.get("id");
                    String title = (String) firstNews.get("title");
                    String resNewsDetail = news.getNewsDetail(id, token);
                    result = news.verifyNewsByTitle(resNewsDetail, title);
                    if(result){
                        int cateId = firstNews.getInt("categoryId");
                        String resInvolveNews = news.getInvolveNews(cateId, id);
                        result = news.verifyInvolveNews(cateId, new JSONObject(resInvolveNews));
                    }
                }
                reportToExcel(result,i, refs, news.getResultMessage());
                reportToExcel2(result, i, news.jsonInput, news.jsonExpect);
            }
        }
    }
}
