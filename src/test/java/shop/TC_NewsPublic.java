package shop;

import apis.shop.NewsPublic;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_NewsPublic extends BaseTest {
    public TC_NewsPublic(){
        super.sheetName = "NewsPublic";
    }

    @Parameters("refs")
    @Test
    public void TestNewsPublic(@Optional("All") String reference){
        NewsPublic news = new NewsPublic();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String response = news.run(i, "",sheetName, new JSONObject());
                boolean result = news.verifyResponse(i, response, sheetName);
                if(result && response.contains("title")){
                    JSONObject firstNews = news.getFirstNews(response);
                    int id = (int) firstNews.get("id");
                    String title = (String) firstNews.get("title");
                    String resNewsDetail = news.getNewsDetail(id);
                    result = news.verifyNewsByTitle(resNewsDetail, title);
                    if(result){
                        int cateId = firstNews.getInt("categoryId");
                        String resInvolveNews = news.getInvolveNews(cateId, id);
                        result = news.verifyInvolveNews(cateId, new JSONObject(resInvolveNews));
                    }
                }
                reportToExcel(result,i, refs, news.getResultMessage());
                reportToExcel2(result, i, news.jsonInput, news.jsonExpect);
            }
        }
    }
}
