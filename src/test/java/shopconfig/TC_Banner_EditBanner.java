package shopconfig;

import apis.login.Authentication;
import apis.shop.Banner;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Banner_EditBanner extends BaseTest {
    public TC_Banner_EditBanner(){
        super.sheetName = "EditBanner";
    }

    @Parameters("refs")
    @Test
    public void C002_EditBanner(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        Banner banner = new Banner();
        JSONObject data = new JSONObject();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String bannerId = XLSWorker.getValueFromExcel(i, "Id", sheetName);
                String versionNo = banner.getVersionNo(bannerId, token);
                data.put("versionNo", versionNo);
                String resEditBlog = banner.run(i, token, sheetName, data);
                boolean result = banner.verifyResponse(i, resEditBlog, sheetName);
                reportToExcel(result, i, refs, banner.getResultMessage());
                reportToExcel2(result, i, banner.jsonInput, banner.jsonExpect);
            }
        }
    }
}
