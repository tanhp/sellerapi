package shopconfig;

import apis.login.Authentication;
import apis.shop.Banner;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Banner_AddBanner extends BaseTest {
    public TC_Banner_AddBanner(){
        super.sheetName = "AddBanner";
    }

    @Parameters("refs")
    @Test
    public void TestAddBanner(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        Banner banner = new Banner();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String response = banner.run(i, token, sheetName, null);
                boolean result = banner.verifyResponse(i, response, sheetName);
                if(response.contains("id") && result){
                    String bannerId = banner.getBannerId(response);
                    String resBannerList = banner.loadBannerList(token);
                    result = banner.verify(bannerId, resBannerList);
                    if(result)
                        result = banner.deleteBanner(resBannerList, bannerId, token);
                }
                reportToExcel(result, i, refs, banner.getResultMessage());
                reportToExcel2(result, i, banner.jsonInput, banner.jsonExpect);
            }
        }
    }
}
