package shopconfig;

import apis.login.Authentication;
import apis.shop.Blog;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Blog_AddBlog extends BaseTest {
    public TC_Blog_AddBlog(){
        super.sheetName = "AddBlog";
    }

    @Parameters("refs")
    @Test
    public void TestAddBlog(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        Blog blog = new Blog();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String response = blog.run(i, token, sheetName, null);
                boolean result = blog.verifyResponse(i, response, sheetName);
                if(response.contains("id") && result){
                    String blogId = blog.getBlogId(response);
                    String resBlogList = blog.loadBlogList(token);
                    result = blog.verify(blogId, resBlogList);
                    if(result){
                        result = blog.deleteBlog(blogId, token);
                    }
                }
                reportToExcel(result, i, refs, blog.getResultMessage());
                reportToExcel2(result, i, blog.jsonInput, blog.jsonExpect);
            }
        }

    }
}
