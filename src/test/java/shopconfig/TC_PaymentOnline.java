package shopconfig;

import apis.login.Authentication;
import apis.shop.ShopConfig;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_PaymentOnline extends BaseTest {
    public TC_PaymentOnline(){
        super.sheetName = "PaymentOnline";
    }

    @Parameters("refs")
    @Test
    public void TestPaymentOnline(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        ShopConfig shop = new ShopConfig();

        for(int i=1; i<=1; i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                boolean result = shop.loadPaymentOnline(token);
                if(result){
                    String response = shop.run(i, token, sheetName, null);
                    result = shop.verifyResponse(i, response, sheetName);
                }
                reportToExcel(result, i, refs, shop.getResultMessage());
                reportToExcel2(result, i, shop.jsonInput, shop.jsonExpect);
            }
        }
    }
}
