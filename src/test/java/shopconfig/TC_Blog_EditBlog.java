package shopconfig;

import apis.login.Authentication;
import apis.shop.Blog;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Blog_EditBlog extends BaseTest{
    public TC_Blog_EditBlog(){
        super.sheetName = "EditBlog";
    }

    @Parameters("refs")
    @Test
    public void C002_EditBlog(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        Blog blog = new Blog();
        JSONObject data = new JSONObject();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String blogId = XLSWorker.getValueFromExcel(i, "Id", sheetName);
                String resBlogDetail = blog.loadBlogDetail(blogId, token);
                String versionNo = blog.getVersionNo(resBlogDetail);
                data.put("versionNo", versionNo);
                String resEditBlog = blog.run(i, token, sheetName, data);
                boolean result = blog.verifyResponse(i, resEditBlog, sheetName);
                reportToExcel(result, i, refs, blog.getResultMessage());
                reportToExcel2(result, i, blog.jsonInput, blog.jsonExpect);
            }
        }
    }
}
