package order;

import apis.login.Authentication;
import apis.product.Product;
import apis.salesorder.Order;
import object.BaseTest;
import object.Buyer;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Order_MergeOrder extends BaseTest {
    public TC_Order_MergeOrder(){
        super.sheetName = "MergeOrder";
    }

    @Parameters("refs")
    @Test
    public void testMergeOrder(@Optional("All") String reference) throws InterruptedException {
        Authentication authen = new Authentication();
        Buyer buyer = new Buyer();
        Order order = new Order();
        JSONObject data = new JSONObject();
        Product product = new Product();

        for(int i=1 ;i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);

                product.changeStock(1,"20577063", token);

                String resOrderList;
                JSONObject order_obj;

                for(int j=1; j<=2; j++){
                    String resSaveOrder = buyer.saveOrder(141736, 20577063, 100000);
                    long orderNumber = order.getOrderNumberFromCheckout(new JSONObject(resSaveOrder));
                    System.out.println("Order Number: " + orderNumber);

                    Thread.sleep(10000);

                    resOrderList = order.run(3, token, "OrderList", null);
                    order_obj = order.getOrderObject(new JSONObject(resOrderList), orderNumber);
                    long orderId = order.getOrderId(order_obj);
                    data.put("order_" + j, orderId);
                }

                String resMergeOrder = order.run(i, token, sheetName, data);
                boolean result = order.verifyResponse(i, resMergeOrder, sheetName);

                if(result){
//                        resOrderList = order.run(5, token, "OrderList", null);
//                        order_obj = order.getOrderObject(new JSONObject(resOrderList), orderNumber);
//                        result = order.verifyOrderInList(order_obj);
                }
                reportToExcel(result, i, refs, order.getResultMessage());
            }
        }

    }
}
