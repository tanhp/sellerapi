package order;

import apis.login.Authentication;
import apis.product.Product;
import apis.salesorder.Order;
import object.BaseTest;
import object.Buyer;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Order_ConfirmOrder extends BaseTest {

    public TC_Order_ConfirmOrder(){
        super.sheetName = "ConfirmOrder";
    }

    @Parameters("refs")
    @Test
    public void testConfirmOrder(@Optional("All") String reference) throws InterruptedException {
        Authentication authen = new Authentication();
        Buyer buyer = new Buyer();
        Order order = new Order();
        JSONObject data = new JSONObject();
        Product product = new Product();

        for(int i=1 ;i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String shop_id = XLSWorker.getValueFromExcel(1, "shop_id", "Buyer");
                String product_id = XLSWorker.getValueFromExcel(1, "product_id", "Buyer");
                String final_price = XLSWorker.getValueFromExcel(1, "final_price", "Buyer");

                product.changeStock(1, product_id, token);

                String resSaveOrder = buyer.saveOrder(Integer.valueOf(shop_id), Integer.valueOf(product_id), Integer.valueOf(final_price));
                long orderNumber = order.getOrderNumberFromCheckout(new JSONObject(resSaveOrder));
                System.out.println("Order Number: " + orderNumber);

                Thread.sleep(10000);

                String resOrderList = order.run(2, token, "OrderList", null);

                JSONObject order_obj = order.getOrderObject(new JSONObject(resOrderList), orderNumber);
                boolean result = order.verifyOrderInList(order_obj);
                if(result){
                    long orderId = order.getOrderId(order_obj);
                    data.put("orderId", String.valueOf(orderId));
                    String resOrderDetail = order.getOrderDetail(orderId, token);
                    String versionNo = order.getVersionNo(new JSONObject(resOrderDetail).getJSONObject("result").getJSONObject("salesOrder"));
                    data.put("versionNo", versionNo);
                    result = order.getSOGroup(i, sheetName, orderId, versionNo, token);

                    if(result){
                        String resConfirmOrder = order.run(i, token, sheetName, data);
                        result = order.verifyResponse(i, resConfirmOrder, sheetName);
                    }
                    if(result){
                        Thread.sleep(10000);
                        resOrderList = order.run(4, token, "OrderList", null);
                        order_obj = order.getOrderObject(new JSONObject(resOrderList), orderNumber);
                        result = order.verifyOrderInList(order_obj);
                    }
                }
                reportToExcel(result, i, refs, order.getResultMessage());
            }
        }

    }
}
