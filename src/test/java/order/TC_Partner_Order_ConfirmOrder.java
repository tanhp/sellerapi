package order;

import apis.login.Authentication;
import apis.product.Product;
import apis.salesorder.Order;
import object.BaseTest;
import object.Buyer;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Partner_Order_ConfirmOrder extends BaseTest {
    public TC_Partner_Order_ConfirmOrder(){
        super.sheetName = "ConfirmOrder";
    }

    @Parameters("refs")
    @Test
    public void testConfirmOrder(@Optional("All") String reference) throws InterruptedException {
        Authentication authen = new Authentication();
        Buyer buyer = new Buyer();
        Order order = new Order();
        JSONObject data = new JSONObject();
        Product product = new Product();

        for(int i=1 ;i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String bearerToken = authen.getBearerToken(token);

//                String shop_id = XLSWorker.getValueFromExcel(1, "shop_id", "Buyer");
//                String product_id = XLSWorker.getValueFromExcel(1, "product_id", "Buyer");
//                String final_price = XLSWorker.getValueFromExcel(1, "final_price", "Buyer");
//
//                product.changeStock(1, product_id, token);
//
//                String resSaveOrder = buyer.saveOrder(Integer.valueOf(shop_id), Integer.valueOf(product_id), Integer.valueOf(final_price));
//                long orderNumber = order.getOrderNumberFromCheckout(new JSONObject(resSaveOrder));

                String orderNumber = "14219262417";
                data.put("orderNumber", orderNumber);
                System.out.println("Order Number: " + orderNumber);

                Thread.sleep(10000);

                String resConfirmOrder = order.run(i, bearerToken, sheetName, data);
                boolean result = order.verifyResponse(i, resConfirmOrder, sheetName);

                reportToExcel(result, i, refs, order.getResultMessage());
            }
        }

    }
}
