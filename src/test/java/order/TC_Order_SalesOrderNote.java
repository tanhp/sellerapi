package order;

import apis.login.Authentication;
import apis.salesorder.Order;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Order_SalesOrderNote extends BaseTest {
    public TC_Order_SalesOrderNote(){
        super.sheetName = "SalesOrderNote";
    }

    @Parameters("refs")
    @Test
    public void TestSalesOrderNote(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        Order order = new Order();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String orderId = XLSWorker.getValueFromExcel(i,"SalesOrderId",sheetName);
                String response = order.run(i, token, sheetName, null);
                boolean result = order.verifyResponse(i, response, sheetName);
                if(result){
                    int noteId = order.getNoteId(new JSONObject(response));
                    String resNoteList = order.getSalesOrderNotes(Integer.valueOf(orderId), token);
                    result = order.verifyNoteInList(noteId, new JSONObject(resNoteList));
                }
                reportToExcel(result, i, refs, order.getResultMessage());
            }
        }
    }
}
