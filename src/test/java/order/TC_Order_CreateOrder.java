package order;

import apis.login.Authentication;
import apis.product.Product;
import apis.salesorder.Order;
import object.BaseTest;
import object.Buyer;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Order_CreateOrder extends BaseTest {
    public TC_Order_CreateOrder(){
        super.sheetName = "CreateOrder";
    }

    @Parameters("refs")
    @Test
    public void testCreateOrder(@Optional("All") String reference) throws InterruptedException {
        Authentication authen = new Authentication();
        Buyer buyer = new Buyer();
        Order order = new Order();
        Product product = new Product();

        for(int i=1 ;i<=1; i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);

                product.changeStock(1, "20577226", token);

                String resSaveOrder = buyer.saveOrder(138994, 20577226, 100000);
                long orderNumber = order.getOrderNumberFromCheckout(new JSONObject(resSaveOrder));

                Thread.sleep(7000);

                String resOrderList = order.run(3, token, "OrderList", null);

                JSONObject order_obj = order.getOrderObject(new JSONObject(resOrderList), orderNumber);
                boolean result = order.verifyOrderInList(order_obj);
                if(result){
                    long orderId = order.getOrderId(order_obj);
                    String resOrderDetail = order.getOrderDetail(orderId, token);
                    result = order.verifyOrderDetail(orderId, new JSONObject(resOrderDetail));
                }
                reportToExcel(result, i, refs, order.getResultMessage());
            }
        }

    }
}
