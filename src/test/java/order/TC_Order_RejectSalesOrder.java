package order;

import apis.login.Authentication;
import apis.salesorder.Order;
import apis.shop.Customer;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Order_RejectSalesOrder extends BaseTest {
    public TC_Order_RejectSalesOrder(){
        super.sheetName = "RejectSalesOrder";
    }

    @Parameters("refs")
    @Test
    public void TestRejectSalesOrder(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        Order order = new Order();
        Customer customer = new Customer();

        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String orderId = XLSWorker.getValueFromExcel(i,"orderId",sheetName);
                String response = order.run(i, token, sheetName, null);
                boolean result = order.verifyResponse(i, response, sheetName);
                if(result){
                    String resGetOrderDetail = order.getOrderDetail(Long.valueOf(orderId), token);
                    String orderNumber = new JSONObject(resGetOrderDetail).getJSONObject("result").getJSONObject("salesOrder").getString("orderNumber");
                    String resBadBuyerList = customer.getBadBuyerList(token);
                    result = customer.verifyBadBuyerInList(Long.valueOf(orderNumber), new JSONObject(resBadBuyerList));
                    if(result){
                        int badBuyerId = customer.getBadBuyerId(Long.valueOf(orderNumber), new JSONObject(resBadBuyerList));
                        customer.removeBadBuyerBlackList(badBuyerId, token);
                        resBadBuyerList = customer.getBadBuyerList(token);
                        System.out.println("---verifyBadBuyerNotInList---");
                        result = customer.verifyBadBuyerNotInList(Long.valueOf(orderNumber), new JSONObject(resBadBuyerList));
                    }
                }
                reportToExcel(result, i, refs, order.getResultMessage());
                reportToExcel2(result, i, order.jsonInput, order.jsonExpect);
            }
        }
    }
}
