package order;

import apis.login.Authentication;
import apis.salesorder.Order;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Partner_Order_PrintBill extends BaseTest {
    public TC_Partner_Order_PrintBill(){
        super.sheetName = "PrintBill";
    }

    @Parameters("refs")
    @Test
    public void TestOrderList(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        Order order = new Order();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String bearerToken = authen.getBearerToken(token);
                String response = order.run(i, bearerToken, sheetName, null);
                boolean result = order.verifyResponse(i, response, sheetName);
                if(result){
                    String link = new JSONObject(response).getJSONObject("result").getString("data");
                    System.out.println(link);
                    String a = order.run("GET", link, "", "");
                    System.out.println(a);
                }
                reportToExcel(result, i, refs, order.getResultMessage());
            }
        }
    }
}
