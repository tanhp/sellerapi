package order;

import apis.login.Authentication;
import apis.salesorder.Order;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Mobile_Label_AddLabel extends BaseTest {
    public TC_Mobile_Label_AddLabel(){
        super.sheetName = "AddLabel";
    }

    @Parameters("refs")
    @Test
    public void C001_AddLabel(@Optional("All") String reference){
        Authentication authen = new Authentication();
        Order order = new Order();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String response = order.run(i, token, sheetName, null);
                boolean result = order.verifyResponse(i, response, sheetName);
                if(result && response.contains("id")){
                    int labelId = order.getLabelId(new JSONObject(response));
                    String orderId = XLSWorker.getValueFromExcel(i, "OrderId", sheetName);
                    String resLabelList = order.getLabelsByOrderIdOnMobile(orderId, "true", token);
                    result = order.verifyLabelInList(labelId, true, new JSONObject(resLabelList));
                    if(result){
                        result = order.deleteLabelOnMobile(labelId, token);
                        if(result){
                            resLabelList = order.getLabelsByOrderIdOnMobile(orderId, "true", token);
                            result = order.verifyLabelNotInList(labelId, new JSONObject(resLabelList));
                        }
                    }
                }
                reportToExcel(result, i, refs, order.getResultMessage());
            }
        }
    }
}
