package order;

import apis.login.Authentication;
import apis.salesorder.Order;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Label_ManageLabel extends BaseTest {

    public TC_Label_ManageLabel(){
        super.sheetName = "ManageLabel";
    }

    @Parameters("refs")
    @Test
    public void TestManageLabel(@Optional("All") String reference){
        Authentication authen = new Authentication();
        Order order = new Order();

        for(int i=1; i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String response = order.run(i, token, sheetName, null);
                boolean result = order.verifyResponse(i, response, sheetName);
                if(result){
                    String orderId = XLSWorker.getValueFromExcel(i, "OrderId", sheetName);
                    String labelId = XLSWorker.getValueFromExcel(i, "id", sheetName);
                    String isActive = XLSWorker.getValueFromExcel(i, "isActive", sheetName);
                    String resLabelList = order.getLabelsByOrderId(orderId, isActive, token);
                    result = order.verifyLabelInList(Integer.valueOf(labelId), Boolean.valueOf(isActive), new JSONObject(resLabelList));
                }

                reportToExcel(result, i, refs, order.getResultMessage());
            }
        }
    }

}
