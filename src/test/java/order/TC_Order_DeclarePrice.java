package order;

import apis.login.Authentication;
import apis.product.Product;
import apis.salesorder.Order;
import object.BaseTest;
import object.Buyer;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Order_DeclarePrice extends BaseTest {
    public TC_Order_DeclarePrice(){
        super.sheetName = "DeclarePrice";
    }

    @Parameters("refs")
    @Test
    public void testDeclarePrice(@Optional("All") String reference) throws InterruptedException {
        Authentication authen = new Authentication();
        Buyer buyer = new Buyer();
        Order order = new Order();
        JSONObject data = new JSONObject();
        Product product = new Product();

        for(int i=1 ;i<=sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String shop_id = XLSWorker.getValueFromExcel(2, "shop_id", "Buyer");
                String product_id = XLSWorker.getValueFromExcel(2, "product_id", "Buyer");
                String final_price = XLSWorker.getValueFromExcel(2, "final_price", "Buyer");

                product.changeStock(2, product_id, token);

                String resSaveOrder = buyer.saveOrder(Integer.valueOf(shop_id), Integer.valueOf(product_id), Integer.valueOf(final_price));
                long orderNumber = order.getOrderNumberFromCheckout(new JSONObject(resSaveOrder));
                System.out.println("Order Number: " + orderNumber);

                Thread.sleep(10000);

                String resOrderList = order.run(2, token, "OrderList", null);

                JSONObject order_obj = order.getOrderObject(new JSONObject(resOrderList), orderNumber);
                boolean result = order.verifyOrderInList(order_obj);
                if(result){
                    data.put("orderId", String.valueOf(order.getOrderId(order_obj)));
                    String response = order.run(i, token, sheetName, data);
                    result = order.verifyResponse(i, response, sheetName);
                }
                reportToExcel(result, i, refs, order.getResultMessage());
            }
        }

    }
}
