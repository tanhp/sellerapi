package order;

import apis.login.Authentication;
import apis.salesorder.Order;
import object.BaseTest;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class TC_Order_PrintOrder extends BaseTest {
    public TC_Order_PrintOrder(){
        super.sheetName = "PrintOrder";
    }

    @Parameters("refs")
    @Test
    public void TestOrderList(@Optional("All") String reference) {
        Authentication authen = new Authentication();
        Order order = new Order();
        for(int i=1; i <= sheet.getLastRowNum(); i++){
            String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
            if(reference.equals("All") || reference.equals(refs)){
                String token = authen.getToken(i, sheetName);
                String orderId = XLSWorker.getValueFromExcel(i, "orderIds", sheetName);
                boolean result = order.printSalesOrder(orderId, token);
                if(result){
                    String response = order.run(i, token, sheetName, null);
                    result = response.equals("true");
                }
                reportToExcel(result, i, refs, order.getResultMessage());
            }
        }
    }
}
