package object;

import com.jayway.jsonpath.JsonPath;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.APIController;
import utils.Constants;
import utils.Helper;
import utils.XLSWorker;

import java.text.SimpleDateFormat;
import java.util.*;

public class BaseObject{
    protected String message = "";
    protected String voucherCode;
    public XSSFWorkbook header;
    public XSSFWorkbook workbook;
    public XSSFSheet sheet = null;
    public String jsonInput = "";
    public String jsonExpect = "";
    JSONArray array = new JSONArray();
    JSONArray carriers = new JSONArray();
    JSONObject mObj = new JSONObject();
    String token;
    boolean isArray = false;
    String mainKey = "";
    String phoneNumber;
    public String orderNumber = null;

    public BaseObject(){
        System.out.println("File name: " + BaseTest.fileNameFromBaseTest);
        workbook = XLSWorker.getInternalWorkbook(Constants.INPUT_PATH + BaseTest.fileNameFromBaseTest);

        String headerFile;
        if(BaseTest.fileNameFromBaseTest.contains("sku"))
            headerFile = "header_sku.xlsx";
        else if(BaseTest.fileNameFromBaseTest.contains("shopconfig"))
            headerFile = "header_shopconfig.xlsx";
        else if(BaseTest.fileNameFromBaseTest.contains("order"))
            headerFile = "header_order.xlsx";
        else if(BaseTest.fileNameFromBaseTest.contains("shop"))
            headerFile = "header_shop.xlsx";
        else
            headerFile = "header_oms.xlsx";
        header = XLSWorker.getInternalWorkbook(Constants.INPUT_PATH + headerFile);
    }

    public String getSellerUrl(String value){
        if(BaseTest.fileNameFromBaseTest.contains("staging")){
            switch(value){
                case "BASE_WEB_URL":
                    return  "https://ban-stg.sendo.vn";
                case "BASE_URL":
                    return  "https://seller-api-stg.sendo.vn";
                case "BASE_URL_AUTHEN":
                    return  "https://seller-api-stg.sendo.vn";
                case "BASE_OMS_URL":
                    return  "https://business-api-opc-stg.sendo.vn";
                case "BASE_OPC_URL":
                    return "https://static-opc-stg.sendo.vn";
                case "BASE_BUYER_URL":
                    return "https://stg.sendo.vn";
                case "BASE_CHECKOUT_URL":
                    return "https://checkout-stg.sendo.vn";
                case "BASE_MAPI_URL":
                    return "https://mapi-stg.sendo.vn";
                case "BASE_SAPI_URL":
                    return "https://sapi-stg.sendo.vn";
            }
        }else if(BaseTest.fileNameFromBaseTest.contains("production")){
            switch(value){
                case "BASE_WEB_URL":
                    return  "https://ban.sendo.vn";
                case "BASE_URL":
                    return  "https://seller-api.sendo.vn";
                case "BASE_URL_AUTHEN":
                    return  "https://seller-api.sendo.vn";
                case "BASE_BUYER_URL":
                    return "https://www.sendo.vn";
                case "BASE_CHECKOUT_URL":
                    return "https://checkout.sendo.vn";
                case "BASE_MAPI_URL":
                    return "https://mapi.sendo.vn";
                case "BASE_SAPI_URL":
                    return "https://sapi.sendo.vn";
            }
        }else if(BaseTest.fileNameFromBaseTest.contains("test")){
            switch(value){
                case "BASE_WEB_URL":
                    return  "http://ban.test.sendo.vn";
                case "BASE_URL":
                    return  "http://apipwa.seller.test.sendo.vn";
                case "BASE_URL_AUTHEN":
                    return  "http://apipwa.seller.test.sendo.vn";
                case "BASE_OMS_URL":
                    return  "http://businessapi.opc.test.sendo.vn";
                case "BASE_SAPI_URL":
                    return "http://api.partner.seller.test.sendo.vn";
            }
        }
        String baseApi = XLSWorker.getValueFromExcel(header, value,1, "SellerUrl");
        String endPoint = XLSWorker.getValueFromExcel(header, value,2, "SellerUrl");
        if(BaseTest.fileNameFromBaseTest.contains("staging")){
            switch(baseApi){
                case "BASE_WEB_URL":
                    baseApi =  "https://ban-stg.sendo.vn";
                    break;
                case "BASE_URL":
                    baseApi =  "https://seller-api-stg.sendo.vn";
                    break;
                case "BASE_URL_AUTHEN":
                    baseApi =  "https://seller-api-stg.sendo.vn";
                    break;
                case "BASE_OMS_URL":
                    baseApi =  "https://business-api-opc-stg.sendo.vn";
                    break;
                case "BASE_OPC_URL":
                    baseApi = "https://static-opc-stg.sendo.vn";
                    break;
                case "BASE_BUYER_URL":
                    baseApi = "https://stg.sendo.vn";
                    break;
                case "BASE_CHECKOUT_URL":
                    baseApi = "https://checkout-stg.sendo.vn";
                    break;
                case "BASE_MAPI_URL":
                    baseApi = "https://mapi-stg.sendo.vn";
                    break;
                case "BASE_SAPI_URL":
                    baseApi = "https://sapi-stg.sendo.vn";
                    break;
            }
        }else if(BaseTest.fileNameFromBaseTest.contains("production")){
            switch(baseApi){
                case "BASE_WEB_URL":
                    baseApi = "https://ban.sendo.vn";
                    break;
                case "BASE_URL":
                    baseApi = "https://seller-api.sendo.vn";
                    break;
                case "BASE_URL_AUTHEN":
                    baseApi = "https://seller-api.sendo.vn";
                    break;
                case "BASE_BUYER_URL":
                    baseApi = "https://www.sendo.vn";
                    break;
                case "BASE_CHECKOUT_URL":
                    baseApi = "https://checkout.sendo.vn";
                    break;
                case "BASE_MAPI_URL":
                    baseApi = "https://mapi.sendo.vn";
                    break;
                case "BASE_SAPI_URL":
                    baseApi = "https://sapi.sendo.vn";
                    break;
            }
        }else if(BaseTest.fileNameFromBaseTest.contains("test")){
            System.out.println(baseApi);
            switch(baseApi){
                case "BASE_WEB_URL":
                    baseApi = "http://ban.test.sendo.vn";
                    break;
                case "BASE_URL":
                    baseApi = "http://apipwa.seller.test.sendo.vn";
                    break;
                case "BASE_URL_AUTHEN":
                    System.out.println("BBBBBBBBBB");
                    baseApi = "http://apipwa.seller.test.sendo.vn";
                    break;
                case "BASE_OMS_URL":
                    baseApi = "http://businessapi.opc.test.sendo.vn";
                    break;
                case "BASE_SAPI_URL":
                    baseApi = "http://api.partner.seller.test.sendo.vn";
                    break;
            }
        }
        return baseApi + endPoint;
    }

    public String getSellerUrl(XSSFWorkbook header, String value){
        if(BaseTest.fileNameFromBaseTest.contains("staging")){
            switch(value){
                case "BASE_WEB_URL":
                    return  "https://ban-stg.sendo.vn";
                case "BASE_URL":
                    return  "https://seller-api-stg.sendo.vn";
                case "BASE_URL_AUTHEN":
                    return  "https://seller-api-stg.sendo.vn";
                case "BASE_OMS_URL":
                    return  "https://business-api-opc-stg.sendo.vn";
                case "BASE_OPC_URL":
                    return "https://static-opc-stg.sendo.vn";
                case "BASE_BUYER_URL":
                    return "https://stg.sendo.vn";
                case "BASE_CHECKOUT_URL":
                    return "https://checkout-stg.sendo.vn";
                case "BASE_MAPI_URL":
                    return "https://mapi-stg.sendo.vn";
                case "BASE_SAPI_URL":
                    return "https://sapi-stg.sendo.vn";
            }
        }else if(BaseTest.fileNameFromBaseTest.contains("production")){
            switch(value){
                case "BASE_WEB_URL":
                    return  "https://ban.sendo.vn";
                case "BASE_URL":
                    return  "https://seller-api.sendo.vn";
                case "BASE_URL_AUTHEN":
                    return  "https://seller-api.sendo.vn";
                case "BASE_BUYER_URL":
                    return "https://www.sendo.vn";
                case "BASE_CHECKOUT_URL":
                    return "https://checkout.sendo.vn";
                case "BASE_MAPI_URL":
                    return "https://mapi.sendo.vn";
                case "BASE_SAPI_URL":
                    return "https://sapi.sendo.vn";
            }
        }else if(BaseTest.fileNameFromBaseTest.contains("test")){
            switch(value){
                case "BASE_WEB_URL":
                    return  "http://ban.test.sendo.vn";
                case "BASE_URL":
                    return  "http://apipwa.seller.test.sendo.vn";
                case "BASE_URL_AUTHEN":
                    return  "http://apipwa.seller.test.sendo.vn";
                case "BASE_OMS_URL":
                    return  "http://businessapi.opc.test.sendo.vn";
                case "BASE_SAPI_URL":
                    return "http://api.partner.seller.test.sendo.vn";
            }
        }
        String baseApi = XLSWorker.getValueFromExcel(header, value,1, "SellerUrl");
        String endPoint = XLSWorker.getValueFromExcel(header, value,2, "SellerUrl");
        if(BaseTest.fileNameFromBaseTest.contains("staging")){
            switch(baseApi){
                case "BASE_WEB_URL":
                    baseApi =  "https://ban-stg.sendo.vn";
                    break;
                case "BASE_URL":
                    baseApi =  "https://seller-api-stg.sendo.vn";
                    break;
                case "BASE_URL_AUTHEN":
                    baseApi =  "https://seller-api-stg.sendo.vn";
                    break;
                case "BASE_OMS_URL":
                    baseApi =  "https://business-api-opc-stg.sendo.vn";
                    break;
                case "BASE_OPC_URL":
                    baseApi = "https://static-opc-stg.sendo.vn";
                    break;
                case "BASE_BUYER_URL":
                    baseApi = "https://stg.sendo.vn";
                    break;
                case "BASE_CHECKOUT_URL":
                    baseApi = "https://checkout-stg.sendo.vn";
                    break;
                case "BASE_MAPI_URL":
                    baseApi = "https://mapi-stg.sendo.vn";
                    break;
                case "BASE_SAPI_URL":
                    baseApi = "https://sapi-stg.sendo.vn";
                    break;
            }
        }else if(BaseTest.fileNameFromBaseTest.contains("production")){
            switch(baseApi){
                case "BASE_WEB_URL":
                    baseApi = "https://ban.sendo.vn";
                    break;
                case "BASE_URL":
                    baseApi = "https://seller-api.sendo.vn";
                    break;
                case "BASE_URL_AUTHEN":
                    baseApi = "https://seller-api.sendo.vn";
                    break;
                case "BASE_BUYER_URL":
                    baseApi = "https://www.sendo.vn";
                    break;
                case "BASE_CHECKOUT_URL":
                    baseApi = "https://checkout.sendo.vn";
                    break;
                case "BASE_MAPI_URL":
                    baseApi = "https://mapi.sendo.vn";
                    break;
                case "BASE_SAPI_URL":
                    baseApi = "https://sapi.sendo.vn";
                    break;
            }
        }else if(BaseTest.fileNameFromBaseTest.contains("test")){
            switch(baseApi){
                case "BASE_WEB_URL":
                    baseApi = "http://ban.test.sendo.vn";
                    break;
                case "BASE_URL":
                    baseApi = "http://apipwa.seller.test.sendo.vn";
                    break;
                case "BASE_URL_AUTHEN":
                    baseApi = "http://apipwa.seller.test.sendo.vn";
                    break;
                case "BASE_OMS_URL":
                    baseApi = "http://businessapi.opc.test.sendo.vn";
                    break;
                case "BASE_SAPI_URL":
                    baseApi = "http://api.partner.seller.test.sendo.vn";
                    break;
            }
        }
        return baseApi + endPoint;
    }


    public String getRandom(){
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        Date date = new Date();
        String output = formatter.format(date);
        return output;
    }

    public String getRandom2(){
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMHHmmss");
        Date date = new Date();
        return formatter.format(date);
    }

    public String getCurrentDateTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return formatter.format(date);
    }

    public String getTomorrow() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);
        date = c.getTime();
        String output = formatter.format(date);
        return output;
    }

    public String getToday(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String output = formatter.format(date);
        return output;
    }

    public String getDays(int amount){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, amount);
        String output = formatter.format(cal.getTime());
        return output;
    }

    public String getPromotionDate(String date){
        switch(date){
            case "TODAY":
                return getToday();
            case "TOMORROW":
                return getTomorrow();
            case "GETDAYSBEFORE":
                return getDays(-7);

                default:
                    return date;
        }
    }

    public String getParamsFromExcel(String symbol, int row, String sheetName, JSONObject data){
        int index1 = XLSWorker.getColumnNumByName("Token",sheetName);
        int index2 = XLSWorker.getColumnNumByName("StatusCode", sheetName);

        String param = "";
        if(symbol.equals("?")){
            sheet = XLSWorker.getSheet(workbook,sheetName);
            Object[][] object = XLSWorker.getData(workbook,sheetName);
            if(object == null)
                return "";
            for(int j = index1 + 1; j< index2; j++){
                String value = getValue(object[row][j].toString(), data);
                if(value!=null){
                    object[row][j] = value;
                }
                if(!object[row][j].toString().equals("N/A")){
                    if(j != index1 + 1)
                        param = param + "&";
                    param = param + object[0][j].toString() + "=" + object[row][j].toString();
                }
            }
        }else{
            sheet = XLSWorker.getSheet(workbook,sheetName);
            Object[][] object = XLSWorker.getData(workbook,sheetName);
            if(object == null)
                return "";
            for(int j = index1 + 1; j< index2; j++){
                String value = getValue(object[row][j].toString(), data);
                if(value!=null){
                    object[row][j] = value;
                }
                if(!object[row][j].toString().equals("N/A")){
                    param = object[row][j].toString();
                }
            }
        }
        return param;
    }

    public String getValue(String value, JSONObject data){
        switch(value){
            case "GetSkuRandom":
                return "SKU" + getRandom();
            case "GetOrderRandom":
                if(orderNumber==null){
                    orderNumber = "OT" + getRandom();
                }
                return orderNumber;
            case "GetLabelRandom":
                return "L" + getRandom();
            case "GetCurrentDateTime":
                return getCurrentDateTime();
            case "Get48HoursAgo":
                return getDays(-2);
            case "TODAY":
                return getToday();
            case "TOMORROW":
                return getTomorrow();
            case "GETDAYSBEFORE":
                return getDays(-7);
            case "GetProductId":
                return data.getString("productId");
            case "GetOrderId":
                return data.getString("orderId");
            case "GetOrderNumber":
            case "GetOldOrderNumber":
                return data.getString("orderNumber");
                default:
                    if(value.contains("CreateOrder"))
                        return data.getString("orderNumber");
                    else
                        return null;
        }

    }

    public String getPayloadFromExcel(int row, String sheetName, JSONObject data){
        System.out.println("Start get payload with sheetName: " + sheetName);
        JSONObject objectJson = new JSONObject();
        sheet = XLSWorker.getSheet(workbook,sheetName);
        Object[][] object = XLSWorker.getData(workbook,sheetName);

        int index1 = XLSWorker.getColumnNumByName("Token",sheetName);
        int index2 = XLSWorker.getColumnNumByName("StatusCode", sheetName);

        if(object == null)
            return "";
        for(int j = index1 + 1; j< index2; j++){

            if(object[row][j].toString().equals("N/A"))
                continue;

            String value = getValue(object[row][j].toString(), data);
            if(value!=null){
                object[row][j] = value;
            }

            if(sheetName.equals("Register_Step1") && object[0][j].equals("phone")){
                if(object[row][j].equals("GetPhoneNumber")){
                    object[row][j] = "079" + generateRandomIntIntRange(1000000,9999999);
                }
                phoneNumber = object[row][j].toString();
            }

            if(sheetName.equals("Merge_Order")){

                if(object[row][j].toString().startsWith("[") && object[row][j].toString().endsWith("]")){
                    object[row][j] = new JSONArray(object[row][j].toString());
                }
                if(object[row][j].toString().startsWith("{") && object[row][j].toString().endsWith("}")){
                    object[row][j] = new JSONObject(object[row][j].toString());
                }

                if(object[0][j].equals("SaleOrderNumberList") && object[row][j].equals("GetOrderList")){
                    JSONArray array = new JSONArray();
                    array.put(data.get("orderNumber1"));
                    array.put(data.get("orderNumber2"));
                    objectJson.put(object[0][j].toString(), array);
                    continue;
                }

                if(object[0][j].equals("UserMerge")){
                    objectJson.put("UserMerge",object[row][j]);
                    continue;
                }

                mObj.put(object[0][j].toString(), object[row][j]);
                if(object[0][j].equals("IsBadBuyerOrder")){
                    objectJson.put("NewSaleOrderObj",mObj);
                }

                continue;

            }

            if(sheetName.equals("Split_Order")){

                if(object[row][j].toString().startsWith("[") && object[row][j].toString().endsWith("]")){
                    object[row][j] = new JSONArray(object[row][j].toString());
                }
                if(object[row][j].toString().startsWith("{") && object[row][j].toString().endsWith("}")){
                    object[row][j] = new JSONObject(object[row][j].toString());
                }

                if(object[0][j].equals("ParentOrderNumber")){
                    objectJson.put(object[0][j].toString(), data.get("orderNumber1"));
                    continue;
                }

                if(object[0][j].equals("UserSplit")){
                    objectJson.put("UserSplit",object[row][j]);
                    continue;
                }

                mObj.put(object[0][j].toString(), object[row][j]);
                if(object[0][j].equals("ShipmentTransportType")){
                    objectJson.put("SalesOrder",mObj);
                }

                continue;

            }


            if(object[row][j].equals("GetVersionNo")){
                object[row][j] = data.get("versionNo");
            }

            if(object[row][j].equals("GetProductId")){
                object[row][j] = data.get("productId");
            }

            if(object[row][j].equals("GetOrderId")){
                object[row][j] = data.get("orderId");
            }

            if(object[row][j].equals("GetTrackingNumber")){
                object[row][j] = "SGT" + orderNumber;
            }

            if(object[row][j].equals("GetShopId")){
                object[row][j] = data.get("shopId");
            }

            if(object[row][j].equals("GetBuyerId")){
                object[row][j] = data.get("buyerId");
            }

            if(object[row][j].equals("GetOtp")){
                object[row][j] = getOTP(phoneNumber);
            }

            if(object[row][j].equals("GetPageSize")){
                object[row][j] = getPageSize();
            }

            if(sheetName.equals("Installment")){
                if(object[0][j].equals("InstallmentTerms")){
                    array.put(new JSONObject(object[row][j].toString()));
                    objectJson.put(object[0][j].toString(),array);
                    continue;
                }
            }

            if(sheetName.equals("PaymentOnline")){
                if(object[0][j].equals("SenpayLevel")){
                    array.put(new JSONObject(object[row][j].toString()));
                    objectJson.put(object[0][j].toString(),array);
                    continue;
                }
            }

            if(sheetName.equals("ShippingConfig")){
                if(object[0][j].equals("Levels")){
                    array.put(new JSONObject(object[row][j].toString()));
                    objectJson.put(object[0][j].toString(),array);
                    continue;
                }
            }

            if(sheetName.equals("Carrier")){
                array.put(new JSONObject(object[row][j].toString()));
                if(object[0][j].toString().contains("instant"))
                    return array.toString();
            }

            if(sheetName.equals("DropOff")){
                if(object[0][j].equals("Carriers")){
                    array.put(new JSONObject(object[row][j].toString()));
                    objectJson.put(object[0][j].toString(),array);
                    continue;
                }
            }

            if(sheetName.equals("NotiPrivateOffer")){
                array.put(new JSONObject(object[row][j].toString()));
                if(object[0][j].toString().contains("Noti3"))
                    return array.toString();
            }

            if(object[row][j].toString().startsWith("[") && object[row][j].toString().endsWith("]")){
                objectJson.put(object[0][j].toString(), new JSONArray(object[row][j].toString()));
                continue;
            }
            if(object[row][j].toString().startsWith("{") && object[row][j].toString().endsWith("}")){
                objectJson.put(object[0][j].toString(), new JSONObject(object[row][j].toString()));
                continue;
            }


            if(sheetName.contains("AddProduct") || sheetName.contains("EditProduct") || sheetName.contains("Draft") || sheetName.contains("GTKTT")){
                if(object[0][j].equals("ProductImage") && object[row][j].equals("GET")){
                    objectJson.put("ProductImage", data.get("productImage"));
                    continue;
                }
                if(object[0][j].equals("Attributes")){
                    objectJson.put("Attributes", new JSONArray((String) data.get("attributes")));
                    continue;
                }
                if(object[0][j].equals("BrandName") && !object[row][j].equals("")){
                    objectJson.put("BrandId", data.get("brandId"));
                    continue;
                }

                if(object[0][j].equals("Category")){
                    objectJson.put("Cat2Id", readValueByJsonPath((String) data.get("cateResponse"), XLSWorker.getValueFromExcel(header,"Cate2Id", 1, "JsonPath")));
                    objectJson.put("Cat3Id", readValueByJsonPath((String) data.get("cateResponse"), XLSWorker.getValueFromExcel(header,"Cate3Id", 1, "JsonPath")));
                    objectJson.put("Cat4Id", readValueByJsonPath((String) data.get("cateResponse"), XLSWorker.getValueFromExcel(header,"Cate4Id", 1, "JsonPath")));
                    continue;
                }

                if(object[0][j].equals("Cat4Id")){
                    objectJson.put("Cat2Id", data.get("Cat2Id"));
                    objectJson.put("Cat3Id", data.get("Cat3Id"));
                    objectJson.put("Cat4Id", data.get("Cat4Id"));
                    continue;
                }

                if(object[0][j].toString().contains("Attr")){
                    continue;
                }

                if(object[0][j].equals("CertificateFileUpload") && !object[row][j].equals("")){
                    JSONArray certificates = (JSONArray) data.get("certificates");
                    if(certificates.length()==0){
                        JSONArray array = new JSONArray();
                        JSONObject obj = new JSONObject();
                        obj.put("FileName", "ABCD");
                        obj.put("AttachmentUrl","ABCD.docx");
                        obj.put("IsImage",true);
                        array.put(obj);
                        objectJson.put(object[0][j].toString(), array);
                    }else{
                        objectJson.put(object[0][j].toString(), certificates);
                    }
                    continue;
                }
                if(object[0][j].equals("Variants")){
                    objectJson.put("IsConfigVariant", true);
                    objectJson.put("Variants", data.get("variants"));
                    continue;
                }
            }

            if(sheetName.equals("ShopInfo")){
                if(object[0][j].equals("Id") && object[row][j].equals("GET")){
                    objectJson.put("id", data.get("shopId"));
                    continue;
                }
                if(object[0][j].equals("StoreId") && object[row][j].equals("GET")){
                    objectJson.put("storeId", data.get("shopId"));
                    continue;
                }
                if(object[0][j].equals("ShopWardId")){
                    objectJson.put("ShopRegionId", data.get("shopRegionId"));
                    objectJson.put("ShopDistrictId", data.get("shopDistrictId"));
                    objectJson.put("ShopWardId", data.get("shopWardId"));
                    continue;
                }
                if(object[0][j].equals("WarehouseWardId")){
                    objectJson.put("WarehouseRegionId", data.get("warehouseRegionId"));
                    objectJson.put("WarehouseDistrictId", data.get("warehouseDistrictId"));
                    objectJson.put("WarehouseWardId", data.get("warehouseWardId"));
                    System.out.println(data.get("shopId"));
                    continue;
                }
            }


            if(sheetName.equals("ConfirmOrders")){
                if(object[0][j].equals("orders")){
                    JSONObject obj = new JSONObject();
                    obj.put("Id", data.get("orderId"));
                    obj.put("VersionNo", data.get("versionNo"));
                    JSONArray array = new JSONArray();
                    array.put(obj);
                    objectJson.put(object[0][j].toString(), array);
                    continue;
                }
            }

            if(sheetName.equals("MergeOrder")){
                if(object[0][j].equals("salesOrders")){
                    JSONArray array = new JSONArray();
                    array.put(data.get("order_1"));
                    array.put(data.get("order_2"));
                    objectJson.put(object[0][j].toString(), array);
                    continue;
                }
            }

            if(sheetName.contains("PrivateOffer")){
                if(object[0][j].equals("PrivateOfferId") && object[row][j].equals("GET") ){
                    objectJson.put("PrivateOfferId", data.get("privateOfferId"));
                    continue;
                }

                if(object[0][j].equals("VoucherValue") && object[row][j].equals("GET") ){
                    objectJson.put("VoucherValue", data.get("voucherValue"));
                    continue;
                }
            }

            if(sheetName.equals("AddVoucher") || sheetName.equals("EditVoucher")){
                if(object[0][j].equals("Code") && object[row][j].toString().contains("RANDOM")){
                    String prefix = object[row][j].toString().replace("RANDOM","");
                    String random = getRandom2();
                    object[row][j] = prefix + random;
                    objectJson.put("Code", object[row][j].toString());
                    voucherCode = object[row][j].toString();
                    continue;
                }
            }

            if(object[row][j].equals("false") || object[row][j].equals("true")){
                objectJson.put(object[0][j].toString(), Boolean.valueOf(object[row][j].toString()));
                continue;
            }

            objectJson.put(object[0][j].toString(),object[row][j].toString());

        }
        if(sheetName.equals("QuickEdit"))
            return "[" + objectJson.toString() + "]";
        return Helper.decodeUnicodeString(objectJson.toString());
    }

    public String run(int row, String token, String sheetName, JSONObject data){
        String method = XLSWorker.getValueFromExcel(row,"Method",sheetName);
        String source = getSellerUrl(XLSWorker.getValueFromExcel(row, "EndPoint", sheetName));
        String payload = "";
        if(source.endsWith("?")){
            source = source + getParamsFromExcel("?", row, sheetName, data);
        }else if(source.endsWith("/")){
            source = source + getParamsFromExcel("/", row, sheetName, data);
        }else{
            if(!method.equals("GET")){
                payload = getPayloadFromExcel(row, sheetName, data);
            }
        }
        jsonInput = payload;
        return APIController.sendApiRequest(method, source, payload, token);
    }

    public String run(String method, String source, String payload, String token){
        String result = APIController.sendApiRequest(method, source, payload, token);
        if(!result.contains("\"statusCode\":200") || result.contains("Id") || !result.contains("\"IsError\":false") || result.contains("data") || result.contains("increment_id"))
            return result;
        return null;
    }

    public String run(String method, String source, String payload, String token, String contentType){
        String result = APIController.sendApiRequest(method, source, payload, token, contentType);
        if(!result.contains("\"statusCode\":200") || result.contains("Id") || !result.contains("\"IsError\":false") || result.contains("data") || result.contains("increment_id"))
            return result;
        return null;
    }

    public List<String> getFormData(String name, String type, String chunk, String chunks, String albumid){
        List<String> contents = new ArrayList<>();
        contents.add("START");
        contents.add("Content-Disposition: form-data; name=\"name\"");
        contents.add("CRLF");
        contents.add(name);
        if(!chunk.equals("")){
            contents.add("START");
            contents.add("Content-Disposition: form-data; name=\"chunk\"");
            contents.add("CRLF");
            contents.add(chunk);
        }
        if(!chunks.equals("")){
            contents.add("START");
            contents.add("Content-Disposition: form-data; name=\"chunks\"");
            contents.add("CRLF");
            contents.add(chunks);
        }
        if(!albumid.equals("")){
            contents.add("START");
            contents.add("Content-Disposition: form-data; name=\"albumid\"");
            contents.add("CRLF");
            contents.add(albumid);
        }
        contents.add("START");
        contents.add("Content-Disposition: form-data; name=\"file\"; filename=\"" + name + "\"");
        contents.add("Content-Type: " + type );
        contents.add("CRLF");
        contents.add("DATA");
        contents.add("CRLF");
        contents.add("END");
        return contents;
    }

    public String upload(int row, String token, String sheetName, String fileName, String boundaryName, List<String> contents){
        String source = getSellerUrl(XLSWorker.getValueFromExcel(row, "EndPoint", sheetName));
        return APIController.upload(fileName, boundaryName, contents, source, token);
    }

    public boolean verifyResponse(int row, String actualResponse, String sheetName){
        JSONObject json = new JSONObject();
        int index1 = XLSWorker.getColumnNumByName("StatusCode",sheetName);
        int index2 = XLSWorker.getColumnNumByName("Actual Response", sheetName);
        String statusCode = "";
        for(int i = index1 ; i < index2; i++){
            String expectResponseName = XLSWorker.getValueFromExcel(0, i, sheetName);
            if(expectResponseName.contains("Expect"))
                continue;
            String expectResponseValue = XLSWorker.getValueFromExcel(row, i, sheetName);
            if(expectResponseValue.equals("N/A")){
                continue;
            }
            if(!statusCode.equals("200")){
                System.out.println("Expected response result: " + expectResponseValue);
                String jsonPath = XLSWorker.getValueFromExcel(header, expectResponseName,1,"JsonPath");
                System.out.println("Json path: " + jsonPath);
                json.put(jsonPath, expectResponseValue);
                jsonExpect = json.toString();
                String actualResponseValue = readValueByJsonPath(actualResponse, jsonPath);
                System.out.println("Actual response value: " + actualResponseValue);
                if(actualResponseValue==null){
                    jsonPath = XLSWorker.getValueFromExcel(header, "ErrorMessage",1,"JsonPath");
                    actualResponseValue = readValueByJsonPath(actualResponse, jsonPath);
                    message = "Lỗi: " + actualResponseValue;
                    return false;
                }
                if(expectResponseValue.equals("") || !actualResponseValue.contains(expectResponseValue)){
                    message = "Lỗi: " + actualResponseValue;
                    System.out.println(message);
                    return false;
                }
                if(expectResponseName.equals("StatusCode"))
                    statusCode = actualResponseValue;
            }
        }
        return true;
    }

    public String getJsonExpectedResponse(int row, String sheetName){
        int index1 = XLSWorker.getColumnNumByName("StatusCode",sheetName);
        int index2 = XLSWorker.getColumnNumByName("Actual Response", sheetName);
        JSONObject obj = new JSONObject();
        for(int i = index1 ; i < index2; i++){
            String expectResponseName = XLSWorker.getValueFromExcel(0, i, sheetName);
            String expectResponseValue = XLSWorker.getValueFromExcel(row, i, sheetName);
            String jsonPath = XLSWorker.getValueFromExcel(expectResponseName,1,"JsonPath");
            obj.put(jsonPath, expectResponseValue);
        }
        return obj.toString();
    }

    public String getToken(int row, String sheetName) {
        if(sheetName.equals("Login"))
            return "";
        String payloadToken = XLSWorker.getValueFromExcel(row,"Token",sheetName);
        String endPoint = XLSWorker.getValueFromExcel(row, "EndPoint", sheetName);
        if(!payloadToken.equals("")){
            String baseUrl = getSellerUrl("BASE_URL_AUTHEN");
            String endPoint_token;

            if(endPoint.contains("_MOBILE_")){
                endPoint_token = "/api/Token/mobilev2";
            }else{
                endPoint_token = "/api/token/web";
            }

            String source = baseUrl + endPoint_token;
            return APIController.getAuthorizationCode(source, payloadToken);
        }
        return "";
    }

    public String getBuyerToken() {
        String payloadToken = "{\"password\":\"123456\",\"username\":\"newemail08@gmail.com\"}";
        String source = "https://mapi-stg.sendo.vn/mob/user/login";
        return APIController.getAuthorizationCode(source, payloadToken);
    }

    public String getResultMessage(){
        return message;
    }

    public String readValueByJsonPath(String response, String path){
        try {
            return JsonPath.read(response, path).toString();
        }catch (Exception e){
            return null;
        }
    }

    public static List<String> getValueOfJson(JSONObject input, String _key) {
        List<String> value = new ArrayList<>();
        Iterator<String> keys = input.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (input.get(key) instanceof JSONObject) {
                JSONObject object = input.getJSONObject(key);
                value.addAll(getValueOfJson(object, _key));

            } else {
                if (input.get(key) instanceof JSONArray) {
                    JSONArray array = (JSONArray) input.get(key);
                    for (int i = 0; i < array.length(); i++) {
                        try {
                            JSONObject object = (JSONObject) array.get(i);
                            value.addAll(getValueOfJson(object, _key));
                        } catch (ClassCastException e) {

                        }

                    }
                } else {
                    if (_key.contains(key) && key.contains(_key))
                        value.add(input.get(key).toString());
                }

            }
        }
        return value;
    }

    public JSONObject getPageSize(){
        JSONObject page = new JSONObject();
        JSONObject obj = new JSONObject();
        obj.put("CurrentPage", 1);
        obj.put("PageSize", 10);
        page.put("Page", obj);
        return obj;
    }

    public String getOTP(String phoneNumber){
        String url = "http://bapi.sendo.aws/customer/send-otp";
        JSONObject obj = new JSONObject();
        obj.put("type","register");
        obj.put("phone",phoneNumber);
        obj.put("message","%s la ma xac thuc OTP de dat hang tren Sendo");
        String result = APIController.sendApiRequest("POST",url,obj.toString(),"");
        JSONObject token = new JSONObject(result);
        return (String) token.get("token");
    }

    public static int generateRandomIntIntRange(int min, int max) {
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }


}
