package automationLibrary.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class Database {
	public static String url = "";  
    public static String username = "";
    public static String password = "";
    public static Connection connect;
    public static Statement statement;

    public static Connection getConnection(String url) {
    	System.out.println("Connect to " + url);
    	try {
    		connect = DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
        	System.out.println(ex);
        }
        return connect;
    }
    
    public static Connection getConnection(String url, String username, String password) {
    	System.out.println("Connect to " + url);
    	try {
    		connect = DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
        	System.out.println(ex);
        }
        return connect;
    }
    
    public static void closeConnection() {
    	try {
			connect.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    public static Statement useDatabase(String databaseName) {
    	try {
    		statement = connect.createStatement();
			statement.execute("USE " + databaseName);
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	return statement;
    }
    
	public static void insertIntoDatabase(String tableName, List<String> columns, List<String> values) {
		String sql = "INSERT INTO " + tableName + "(";
		for (int i = 0; i < columns.size(); i++) {
			if(i == columns.size() - 1) {
				sql = sql + columns.get(i);
			} else {
				sql = sql + columns.get(i) + " , ";
			}
		}
		sql = sql + ") VALUES('";

		for (int i = 0; i < values.size(); i++) {
			if(i == values.size() - 1) {
				sql = sql + values.get(i);
			} else {
				sql = sql + values.get(i) + "' , '";
			}
				
		}
		sql = sql + "')";
		System.out.println(sql);
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void updateDatabase(String tableName, List<String> updateColumns, List<String> conditionColumns) {
		System.out.println("Start to update into db...");
		String sql = "UPDATE " + tableName + " SET ";
		for(int i = 0; i < updateColumns.size(); i++) {
			String[] splitUpdateColumn = updateColumns.get(i).split(" , ");
			if(updateColumns.size() == 1) {
				sql = sql + splitUpdateColumn[0] + " = '" + splitUpdateColumn[1] + "'";
			} else {
				if(i < updateColumns.size() - 1) {
					sql = sql + splitUpdateColumn[0] + " = '" + splitUpdateColumn[1] + "'" + ", ";
				} else {
					sql = sql + splitUpdateColumn[0] + " = '" + splitUpdateColumn[1] + "'";
				}
			}
		}
		
		if(conditionColumns != null) {
			sql = sql + " WHERE ";
			for(int i = 0; i < conditionColumns.size(); i++) {
				String[] splitConditionColumn = conditionColumns.get(i).split(" , ");
				if(conditionColumns.size() == 1) {
					sql = sql + splitConditionColumn[0] + " = '" + splitConditionColumn[1] + "'";
				} else {
					if(i < conditionColumns.size() - 1) { 
						sql = sql + splitConditionColumn[0] + " = '" + splitConditionColumn[1] + "'" + " AND ";
					} else {
						sql = sql + splitConditionColumn[0] + " = '" + splitConditionColumn[1] + "'";
					}
				}
			}
		}
		
		sql = sql + ";";
		try {
			System.out.println(sql);
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static ResultSet select(String sql) {
    	ResultSet rs = null;
    	try {
			rs = statement.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    	return rs;
    }
}
