package automationLibrary.initiations;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import automationLibrary.actions.ActionGen;
import automationLibrary.data.Data;
import automationLibrary.data.TestCase;
import automationLibrary.database.Database;

public class Configurations {

	public static final String configFileStr = System.getProperty("user.dir") + "//Config.properties";
	public static final String jsonFile = System.getProperty("user.dir") + "//data//TestData.json";
	public static File configFile = new File(configFileStr);

	// Database
	public static String databaseUrl26 = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "db_url_26");
	public static String databaseUrl45 = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "db_url_45");
	public static String databaseUrl47 = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "db_url_47");
	public static String databaseName = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "db_name");
	public static String databaseUsername = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "db_username");
	public static String databasePassword = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "db_password");
	public static String updatedTable = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "updated_table");

	public static String realTimeDatabase = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "realtime_db");
	public static String realTimeTable = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "realtime_table");
	public static String realTimeProductionTable = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "realtime_production_table");

	// Timezone offset
	public static int timezoneOffset = Integer.parseInt(ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "timezone_offset"));

	// Environment
	public static String environment = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "environment");

	// Realtime run
	public static boolean realTimeRun = Boolean.parseBoolean(ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "realtime_run"));

	// Extent report
	public static String reportPath = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "report_path");
	public static String reportUrl = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "report_url");
	public static String reportScreenshotPath = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "report_screenshot_path");

	// Email
	public static String userMail = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "user_automation_mail");
	public static String passwordMail = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "password_automation_mail");
	public static String recipientMail = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "recipient_mail");

	// Priority percentage
	public static double maxFailedP1Percentage = Double
			.valueOf(ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "max_failed_p1_percentage"));
	public static double maxFailedP2Percentage = Double
			.valueOf(ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "max_failed_p2_percentage"));
	public static double maxFailedP3Percentage = Double
			.valueOf(ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "max_failed_p3_percentage"));
	public static double maxFailedP4Percentage = Double
			.valueOf(ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "max_failed_p4_percentage"));

	// Skype
	public static String skypeBotUsername = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(),
			"bot_username");
	public static String skypeBotPassword = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(),
			"bot_password");
	public static String skypeBotGroup = ActionGen.getPropertyFileValue(configFile.getAbsolutePath(), "skype_group_id");

	public static String testRunId = "4452";

	public static void initTestData(String testRunId, String testSuiteName, String featureName, String className, String methodName) {
		System.out.println("TestRunID: " + testRunId);
		System.out.println("TestSuiteName: " + testSuiteName);
		System.out.println("FeatureName: " + featureName);
		System.out.println("ClassName: " + className);
		System.out.println("MethodName: " + methodName);

		List<TestCase> testcaseList = new ArrayList<TestCase>();
		String sql = "SELECT * FROM testcases JOIN testexecutions ON testcases.id = testexecutions.testCaseID JOIN testsuites ON testsuites.id = testcases.testSuiteID WHERE testexecutions.testRunID = '"
				+ testRunId + "' AND testsuites.testSuite = '" + testSuiteName + "'";
		ResultSet resultSet = Database.select(sql);
		try {
			while (resultSet.next()) {
				TestCase testcase = new TestCase();
				testcase.setFeatureName(featureName);
				testcase.setClassName(className);
				testcase.setMethodName(methodName);
				System.out.println("TestcaseID : " + resultSet.getString("id"));
				testcase.setTestCaseId(resultSet.getString("id"));
				testcase.setTestRunId(testRunId);
				//testcase.setInput(new JSONObject(resultSet.getString("testCase")));
				String priority = resultSet.getString("priority");
				if (priority.equals("P1")) {
					testcase.setPriority(ConstantLib.Priority.P1);
				} else if (priority.equals("P2")) {
					testcase.setPriority(ConstantLib.Priority.P2);
				} else if (priority.equals("P3")) {
					testcase.setPriority(ConstantLib.Priority.P3);
				} else if (priority.equals("P4")) {
					testcase.setPriority(ConstantLib.Priority.P4);
				}
				testcase.setRefs(resultSet.getString("refs"));
				testcase.setExpectedResult(resultSet.getString("expectedResult").trim());
				testcaseList.add(testcase);
			}
			Data.testCaseList = testcaseList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
