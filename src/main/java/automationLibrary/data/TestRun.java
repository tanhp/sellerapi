package automationLibrary.data;

public class TestRun {
	public int testRunId;
	public String testRunName;
	
	public int getTestRunId() {
		return testRunId;
	}
	
	public String getTestRunName() {
		return testRunName;
	}
	
	public void setTestRunId(int testRunId) {
		this.testRunId = testRunId;
	}
	
	public void setTestRunName(String testRunName) {
		this.testRunName = testRunName;
	}
}
