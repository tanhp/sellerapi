package automationLibrary.data;

import java.util.List;

public class Data {
    public static List<TestCase> testCaseList;
    public static List<TestSuite> testSuiteList;

    public static String getTestSuiteResult() {
        System.out.println("testCaseList.size: " + testCaseList.size());
        for(int i = 0; i < testCaseList.size(); i++) {
            if(testCaseList.get(i).getTestResult().equals("FAILED")) {
                return "FAILED";
            }
        }
        return "PASSED";
    }

    public static String getFeatureName() {
        return testCaseList.get(0).getFeatureName();
    }

    public static String getClassName() {
        return testCaseList.get(0).getClassName();
    }

    public static String getMethodName() {
        return testCaseList.get(0).getMethodName();
    }
}
