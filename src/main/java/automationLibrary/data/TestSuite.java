package automationLibrary.data;

public class TestSuite {
	public int testRunId;
	public int productId;
	public int featureId;
	public String featureName;
	public String testSuiteName;

	public void setTestRunId(int testRunId) {
		this.testRunId = testRunId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public void setFeatureId(int featureId) {
		this.featureId = featureId;
	}

	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}

	public void setTestSuiteName(String testSuiteName) {
		this.testSuiteName = testSuiteName;
	}

	public int getTestRunId() {
		return testRunId;
	}

	public int getProductId() {
		return productId;
	}

	public int getFeatureId() {
		return featureId;
	}

	public String getFeatureName() {
		return featureName;
	}

	public String getTestSuiteName() {
		return testSuiteName;
	}
}
