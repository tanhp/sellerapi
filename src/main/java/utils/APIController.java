package utils;

import logger.MyLogger;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.testng.annotations.Test;


public class APIController {

    public static String upload(String fileName, String boundaryName, List<String> contents, String source, String authorizationCode){
        System.out.println("************************************************");
        System.out.println("Start to request with authorization ...");
        System.out.println("FileName: " + fileName);
        System.out.println("Source: " + source);
        System.out.println("BoundaryName: " + boundaryName);
        System.out.println("Token: " + authorizationCode);
        assert (source != "" && source != null) : "API link is empty";

        String CRLF = "\r\n"; // Line separator required by multipart/form-data.

        // Task attachments endpoint
        File theFile = new File(Constants.IMAGE_PATH + fileName);

        HttpURLConnection connection = null;


        // A unique boundary to use for the multipart/form-data
        String boundaryNumber = Long.toHexString(System.currentTimeMillis());
        String boundaryValue = boundaryName + boundaryNumber;

        BufferedReader bufReader = null;
        String response = "";
        PrintWriter writer = null;
        try {
            connection = (HttpURLConnection) new URL(source).openConnection();

            // Construct the body of the request
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=----" + boundaryValue);

            // Set basic auth header
            connection.setRequestProperty("Authorization", authorizationCode);

            // Indicate a POST request
            connection.setDoOutput(true);

            if(authorizationCode != null) {
                connection.setRequestProperty("Authorization", authorizationCode);
                connection.setRequestProperty("Cookie","token_seller_api=" + authorizationCode);
            }

            OutputStream output = connection.getOutputStream();
            writer = new PrintWriter(new OutputStreamWriter(output, "UTF-8"));

            for(int i=0; i<contents.size(); i++){
                switch (contents.get(i)){
                    case "START":
                        writer.append("------" + boundaryValue).append(CRLF);
                        break;
                    case "END":
                        writer.append("------" + boundaryValue + "--").append(CRLF).flush();
                        break;
                    case "CRLF":
                        writer.append(CRLF).flush();
                        break;
                    case "DATA":
                        Files.copy(theFile.toPath(), output);
                        break;
                    default:
                        writer.append(contents.get(i)).append(CRLF);
                        break;
                }
            }

            writer.close();

            System.out.println(connection.getResponseCode()); // Should be 200
            System.out.println(connection.getResponseMessage());

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                bufReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            } else {
                bufReader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println("---\nAPI Controller got an " + ioe.getClass().getName() + "\n---");
        } finally {
            try {
                response = bufReader.readLine();
                if (bufReader != null) {
                    bufReader.close();
                }
                connection.disconnect();
            }catch (Exception e){
            }
        }
        if(!response.contains("\"")) {
            response = StringEscapeUtils.unescapeJava(response);
        }
        System.out.println("Get the response after run: " + response);
        System.out.println("************************************************");

        return response;
    }

    public static String sendApiRequest(String method, String source, String payload, String authorizationCode) {
        Helper.sleep(1000);
            MyLogger.info("************************************************");
            MyLogger.info("Start to request with authorization ...");
            MyLogger.info("Method: " + method);
            MyLogger.info("Source: " + source);
            MyLogger.info("Payload: " + payload);
            MyLogger.info("Token: " + authorizationCode);
            assert (source != "" && source != null) : "API link is empty";

            String response = "";
            URL url = null;
            BufferedReader bufReader = null;
            HttpURLConnection conn = null;
            try {
                url = new URL(source);
                conn = (HttpURLConnection) url.openConnection();

                conn.setRequestMethod(method);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept-Charset", "UTF-8");
                conn.setRequestProperty("ReCaptchaResponse", "smc@123456");
                //conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
                if(authorizationCode != null) {
                    conn.setRequestProperty("Authorization", authorizationCode);
                    conn.setRequestProperty("Cookie","token_seller_api=" + authorizationCode);
                }
                conn.setDoOutput(true);
                conn.setDoInput(true);
                if(!method.equals("GET")){
                    if(!payload.contains("N/A")){
                        DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
                        outStream.write(payload.getBytes());
                        outStream.flush();
                        outStream.close();
                    }
                }
                System.out.println("ResponseCode: " + conn.getResponseCode());

                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    bufReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                } else {
                    bufReader = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
                System.out.println("---\nAPI Controller got an " + ioe.getClass().getName() + "\n---");
            } finally {
                try {
                    response = bufReader.readLine();
                    if (bufReader != null) {
                        bufReader.close();
                    }

                    if(true){
                        MyLogger.info("Get the response after run: " + response);
                        MyLogger.info("************************************************");
                    }

                    conn.disconnect();
                }catch (Exception e){
                }
            }
            if(!response.contains("\"")) {
                response = StringEscapeUtils.unescapeJava(response);
            }


        return response;
    }

    public static String sendApiRequest(String method, String source, String payload, String authorizationCode, String contentType) {
        MyLogger.info("************************************************");
        MyLogger.info("Start to request with authorization ...");
        MyLogger.info("Method: " + method);
        MyLogger.info("Source: " + source);
        MyLogger.info("Payload: " + payload);
        MyLogger.info("Token: " + authorizationCode);
        assert (source != "" && source != null) : "API link is empty";

        String response = "";
        URL url = null;
        BufferedReader bufReader = null;
        HttpURLConnection conn = null;
        try {
            url = new URL(source);
            conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod(method);
            conn.setRequestProperty("Content-Type", contentType);
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("ReCaptchaResponse", "smc@123456");
            //conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
            if(authorizationCode != null) {
                conn.setRequestProperty("Authorization", authorizationCode);
                conn.setRequestProperty("Cookie","token_seller_api=" + authorizationCode);
            }
            conn.setDoOutput(true);
            conn.setDoInput(true);
            if(!method.equals("GET")){
                if(!payload.contains("N/A")){
                    DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
                    outStream.write(payload.getBytes(StandardCharsets.UTF_8));
                    outStream.flush();
                    outStream.close();
                }
            }
            System.out.println("ResponseCode: " + conn.getResponseCode());

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                bufReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            } else {
                bufReader = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println("---\nAPI Controller got an " + ioe.getClass().getName() + "\n---");
        } finally {
            try {
                response = bufReader.readLine();
                if (bufReader != null) {
                    bufReader.close();
                }
                conn.disconnect();
            }catch (Exception e){
            }
        }
        if(!response.contains("\"")) {
            response = StringEscapeUtils.unescapeJava(response);
        }

        MyLogger.info("Get the response after run: " + response);
        MyLogger.info("************************************************");
        return response;
    }


    public static String getAuthorizationCode(String source, String payload){
        System.out.println(source);
        System.out.println(payload);
        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost request = new HttpPost(source);
            request.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
            request.setHeader(HttpHeaders.ACCEPT_CHARSET, "UTF-8");
            request.setHeader("ReCaptchaResponse", "smc@123456");
            StringEntity entity = new StringEntity(payload);
            request.setEntity(entity);
            HttpResponse response = httpClient.execute(request);
            String entityResponse = EntityUtils.toString(response.getEntity());
            Object obj = JSONValue.parse(entityResponse);
            JSONObject fullResponse = (JSONObject) obj;
            System.out.println(fullResponse);
            if(fullResponse.containsKey("result")){
                JSONObject result = (JSONObject) fullResponse.get("result");
                if(result.containsKey("token"))
                    return (String) result.get("token");
                else
                    return (String) result.get("access_token");
            }else{
                return (String) fullResponse.get("access_token");
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static Object getValue(String url, String id, String authorizationCode, String key){
        try{
            String source = url + id;
            String response = sendApiRequest("GET",source,"", authorizationCode);
            Object obj = JSONValue.parse(response);
            JSONObject fullResponse = (JSONObject) obj;
            JSONObject result = (JSONObject) fullResponse.get("result");
            return result.get(key);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


}