package apis.shop;

import object.BaseObject;
import org.json.JSONArray;
import org.json.JSONObject;

public class Voucher extends BaseObject {
    private int voucherId;
    private String versionNo;

    public Voucher(){

    }

    public String getVoucherCode(){
        return voucherCode;
    }

    public int getVoucherId(){
        return voucherId;
    }

    public String getVersionNo(){
        return versionNo;
    }

    public boolean verifyVoucherInList(String voucherCode, String response){
        JSONObject obj = new JSONObject(response);
        JSONObject data = (JSONObject) obj.get("Data");
        JSONArray datas = (JSONArray) data.get("Data");
        for(int i=0; i<datas.length(); i++){
            JSONObject voucher = (JSONObject) datas.get(i);
            String code = (String) voucher.get("Code");
            int id = (int) voucher.get("Id");
            String version = (String) voucher.get("VersionNo");
            if(code.equals(voucherCode)){
                voucherId = id;
                versionNo = version;
                return true;
            }
        }
        message = "Cannot find voucher code in list";
        return false;
    }

    public String getVoucherDetail(int voucherId, String token){
        String source = getSellerUrl("ENDPOINT_VOUCHER_VOUCHERDETAIL");
        JSONObject obj = new JSONObject();
        obj.put("Id", voucherId);
        JSONObject page = new JSONObject();
        page.put("CurrentPage", 1);
        page.put("PageSize", 10);
        obj.put("Page", page);
        return run("POST", source, obj.toString(), token);
    }

    public boolean verifyVoucherInDetail(String voucherCode, String response){
        System.out.println("Res: " + response);
        JSONObject obj = new JSONObject(response);
        String code = (String) obj.get("Code");
        if(voucherCode.equals(code))
            return true;
        message = "Cannot find voucher code in detail";
        return false;
    }

    public boolean cancelVoucher(int voucherId, String versionNo, String token){
        String source = getSellerUrl("ENDPOINT_VOUCHER_CANCELVOUCHER");
        JSONObject obj = new JSONObject();
        obj.put("Id", voucherId);
        obj.put("VersionNo", versionNo);
        String response = run("POST", source, obj.toString(), token);
        if(response!=null)
            return true;
        message = "Cancel voucher is failed!";
        return false;
    }
}
