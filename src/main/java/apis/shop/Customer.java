package apis.shop;

import object.BaseObject;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.Constants;
import utils.XLSWorker;

public class Customer extends BaseObject {
    private String endPoint;
    public XSSFWorkbook header;

    public Customer(){
        header = XLSWorker.getInternalWorkbook(Constants.INPUT_PATH + "header_shop.xlsx");
    }

    public String getBadBuyerList(String token){
        String source = getSellerUrl(header, "ENDPOINT_CUSTOMER_GETBADBUYERLIST");
        return run("POST", source, getPageSize().toString(), token);
    }

    public boolean verifyBadBuyerInList(long orderNumber, JSONObject response){
        if(response.getInt("TotalRecord")==0){
            message = "Badbuyer is not in list";
            return false;
        }
        JSONArray data = response.getJSONArray("Data");
        for(int i=0; i<data.length(); i++){
            JSONObject customer = (JSONObject) data.get(i);
            long orderNum = customer.getLong("OrderNumber");
            if(orderNum == orderNumber)
                return true;
        }
        message = "Badbuyer is not in list";
        return false;
    }

    public int getBadBuyerId(long orderNumber, JSONObject response){
        JSONArray data = response.getJSONArray("Data");
        for(int i=0; i<data.length(); i++){
            JSONObject customer = (JSONObject) data.get(i);
            long orderNum = customer.getLong("OrderNumber");
            if(orderNum == orderNumber)
                return customer.getInt("Id");
        }
        return -1;
    }

    public String removeBadBuyerBlackList(int badBuyerId, String token){
        String source = getSellerUrl(header, "ENDPOINT_CUSTOMER_REMOVEBADBUYERBLACKLIST");
        JSONObject obj = new JSONObject();
        obj.put("badBuyerId", badBuyerId);
        return run("POST", source, obj.toString(), token);
    }

    public boolean verifyBadBuyerNotInList(long orderNumber, JSONObject response){
        if(response.getInt("TotalRecord")==0)
            return true;
        JSONArray data = response.getJSONArray("Data");
        for(int i=0; i<data.length(); i++){
            JSONObject customer = (JSONObject) data.get(i);
            long orderNum = customer.getLong("OrderNumber");
            System.out.println(orderNum);
            System.out.println(orderNumber);
            if(orderNum == orderNumber){
                message = "Badbuyer is still in list";
                return false;
            }

        }
        return true;
    }
}
