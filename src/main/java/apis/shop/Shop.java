package apis.shop;

import object.BaseObject;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.APIController;
import utils.XLSWorker;

public class Shop extends BaseObject {
    public String getStoreSettingsNotiPrivateOffer(String token){
        String source = getSellerUrl("ENDPOINT_PRIVATEOFFER_GETNOTIPRIVATEOFFER");
        return run("GET", source, "", token);
    }

    public boolean verifySettingPrivateOffer(JSONObject response){
        JSONArray data = response.getJSONArray("Data");
        if(data.length() == 3)
            return true;
        return false;
    }

    public void validateStoreFunction(String token){
        String source = "https://ban-stg.sendo.vn/Shop/ValidateStoreFunction";
        APIController.sendApiRequest("POST", source, new JSONObject().put("functionCode", "STAFF_PERMISSION").toString(), token, "application/x-www-form-urlencoded");
    }

    public void validateUser(String password, String token){
        String source = getSellerUrl("ENDPOINT_SHOP_VALIDATEUSER");
        run("POST", source, new JSONObject().put("passWord", password).toString(), token);
    }

    public void getStaffPermission(String email, String token){
        String source = getSellerUrl("ENDPOINT_SHOP_GETSTAFFPERMISSION");
        run("POST", source, new JSONObject().put("email", email).toString(), token);
    }

    public void getStaff(String token){
        String source = "https://ban-stg.sendo.vn/StaffPermission/GetStaff";
        run("POST", source, "", token);
    }
}
