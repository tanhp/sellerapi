package apis.shop;

import object.BaseObject;
import org.json.JSONArray;
import org.json.JSONObject;

public class ShopConfig extends BaseObject {
    private String base_api;

    public ShopConfig(){
        base_api = getSellerUrl("BASE_URL");
    }

    public boolean loadInstallment(String token){
        String source = getSellerUrl("ENDPOINT_API_WEB_INSTALLMENT");
        String response = run("GET", source, "N/A", token);
        if(response == null){
            message = "Load installment is failed!";
            return false;
        }
        JSONObject obj = new JSONObject(response);
        JSONObject result = (JSONObject) obj.get("result");
        JSONArray terms = (JSONArray) result.get("installmentTerms");
        if(terms.length() > 0)
            return true;
        message = "Load installment terms is failed!";
        return false;
    }

    public boolean loadPayment(String token){
        String source = getSellerUrl("ENDPOINT_API_WEB_PAYMENT");
        String response = run("GET", source, "N/A", token);
        if(response == null){
            message = "Load payment is failed!";
            return false;
        }
        return true;
    }

    public boolean loadShippingConfig(String token){
        String source = getSellerUrl("ENDPOINT_API_WEB_SUBSCRIBESHIPPINGCONFIG");
        String response = run("GET", source, "N/A", token);
        if(response == null){
            message = "Load shipping config is failed!";
            return false;
        }
        return true;
    }

    public boolean loadPaymentOnline(String token){
        String source = getSellerUrl("ENDPOINT_API_WEB_SUPPORTPAYMENTONLINE");
        String response = run("GET", source, "N/A", token);
        if(response == null){
            message = "Load payment is failed!";
            return false;
        }
        return true;
    }

    public boolean loadMobilePayment(String token){
        String source = getSellerUrl("ENDPOINT_API_WEB_MOBIPAYMENT");
        String response = run("GET", source, "N/A", token);
        if(response == null){
            message = "Load mobile payment is failed!";
            return false;
        }
        return true;
    }

    public boolean loadAllowCheck(String code, String token){
        String source = getSellerUrl("ENDPOINT_API_WEB_STOREALLOWCHECK") + code;
        String response = run("GET", source, "N/A", token);
        if(response == null){
            message = "Load allow check is failed!";
            return false;
        }
        return true;
    }

    public boolean verifyInstallment(){
        return true;
    }
}
