package apis.shop;

import object.BaseObject;
import org.json.JSONArray;
import org.json.JSONObject;

public class News extends BaseObject {

    public News(){

    }

    public JSONObject getFirstNews(String response){
        JSONObject obj = new JSONObject(response);
        JSONObject result = (JSONObject) obj.get("result");
        JSONArray data = (JSONArray) result.get("data");
        if(data.length()==0)
            return null;
        return (JSONObject) data.get(0);
    }

    public String getNewsDetail(int id, String token){
        String source = getSellerUrl("ENDPOINT_API_NEWS_GETNEWSDETAIL") + id;
        return run("GET", source, "", token);
    }

    public String getInvolveNews(int cateId, int newsId){
        String source = getSellerUrl("ENDPOINT_API_NEWSPUBLIC_GETINVOLVENEWS") +  + cateId + "/" + newsId;
        return run("GET", source, "", "");
    }

    public boolean verifyInvolveNews(int cateId, JSONObject obj){
        JSONArray result = obj.getJSONArray("result");
        for(int i=0; i<result.length(); i++){
            JSONObject news = (JSONObject) result.get(i);
            int categoryId = news.getInt("categoryId");
            if(categoryId != cateId){
                message = "CategoryId is not correct!";
                return false;
            }
        }
        return true;
    }

    public int getCategoryId(String response){
        JSONObject obj = new JSONObject(response);
        JSONObject result = (JSONObject) obj.get("result");
        return (int) result.get("categoryId");
    }

    public boolean verifyNewsByTitle(String response, String newsTitle){
        JSONObject obj = new JSONObject(response);
        JSONObject result = (JSONObject) obj.get("result");
        String title = (String) result.get("title");
        if(newsTitle.equals(title))
            return true;
        message = "News title is not correct";
        return false;
    }

}
