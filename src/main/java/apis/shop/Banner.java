package apis.shop;

import object.BaseObject;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Parameters;
import utils.Constants;
import utils.XLSWorker;

public class Banner extends BaseObject {
    String sheetName;

    public Banner(){
        sheetName = "Banner";
    }

    public String getBannerId(String actualResponse){
        String jsonPath = XLSWorker.getValueFromExcel(header,"Id",1,"JsonPath");
        return readValueByJsonPath(actualResponse, jsonPath);
    }

    public String loadBannerList(String token){
        String method = "GET";
        String source = getSellerUrl("ENDPOINT_API_WEB_BANNER");
        return run(method, source, "", token);
    }

    public String getVersionNo(String bannerId, String token){
        String resBannerList = loadBannerList(token);
        JSONObject fullResponse = new JSONObject(resBannerList);
        JSONObject result = (JSONObject) fullResponse.get("result");
        JSONArray datas = (JSONArray) result.get("data");
        for(int i=0; i<datas.length(); i++){
            JSONObject data = (JSONObject) datas.get(i);
            String id = data.get("id").toString();
            if(bannerId.equals(id))
                return (String)data.get("versionNo");
        }
        return null;
    }

    public boolean verify(String bannerId, String responseList){
        if(bannerId==null){
            message = "Cannot get banner Id";
            return false;
        }
        if(responseList==null){
            message = "Cannot get banner list";
            return false;
        }
        JSONObject fullResponse = new JSONObject(responseList);
        JSONObject result = (JSONObject) fullResponse.get("result");
        JSONArray datas = (JSONArray) result.get("data");
        for(int i=0; i<datas.length(); i++){
            JSONObject data = (JSONObject) datas.get(i);
            String id = data.get("id").toString();
            if(bannerId.equals(id))
                return true;
        }
        message = "Cannot find banner Id in banner List!";
        return false;
    }

    public boolean deleteBanner(String response, String bannerId, String token){
        String source = getSellerUrl("ENDPOINT_API_WEB_BANNER");
        JSONObject obj = new JSONObject(response);
        JSONObject result = (JSONObject) obj.get("result");
        JSONArray data = (JSONArray) result.get("data");
        System.out.println(data);
        for(int i=0; i<data.length(); i++){
            JSONObject banner = (JSONObject) data.get(i);
            System.out.println(banner.get("id") + "-" + bannerId);
            if(banner.get("id").toString().equals(bannerId)){
                String resDelBanner = run("DELETE",source, banner.toString(), token);
                if(resDelBanner!=null)
                    return true;
            }
        }
        message = "Delete banner is failed!";
        return false;
    }

}
