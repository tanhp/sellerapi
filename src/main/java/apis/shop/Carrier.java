package apis.shop;

import object.BaseObject;
import org.json.JSONArray;
import org.json.JSONObject;

public class Carrier extends BaseObject {
    public Carrier(){

    }

    public String getCarrierList(String token){
        String source = getSellerUrl("ENDPOINT_SERVICECONFIG_GETCARRIER");
        return run("POST", source, "", token);
    }

    public String getDropOffList(String token){
        String source = getSellerUrl("ENDPOINT_SERVICECONFIG_GETDROPOFF");
        return run("POST", source, "", token);
    }

    public String getSelfTransportList(String token){
        String source = getSellerUrl("ENDPOINT_SERVICECONFIG_GETSELFTRANSPORT");
        return run("POST", source, "", token);
    }

    public boolean verifyCarrierList(String response){
        JSONObject obj = new JSONObject(response);
        JSONArray data = (JSONArray) obj.get("Data");
        if(data.length() > 0)
            return true;
        return false;
    }

    public boolean verifyDropOffList(String response){
        JSONObject obj = new JSONObject(response);
        JSONObject data = (JSONObject) obj.get("Data");
        JSONArray carriers = (JSONArray) data.get("Carriers");
        if(carriers.length() > 0)
            return true;
        return false;
    }

    public boolean verifySelfTransport(String response){
        JSONObject obj = new JSONObject(response);
        JSONObject data = (JSONObject) obj.get("Data");
        JSONArray carriers = (JSONArray) data.get("DeliveryCarrierArea");
        if(carriers.length() > 0)
            return true;
        return false;
    }
}
