package apis.salesorder;

import object.BaseObject;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.XLSWorker;

public class Order extends BaseObject {

    public Order(){
    }

    public JSONObject getOrderCount(String token){
        String source = getSellerUrl("ENDPOINT_SALESORDER_COUNT");
        JSONObject obj = new JSONObject();
        obj.put("orderDateFrom","01-01-2020");
        obj.put("orderDateTo", getToday());
        String response = run("POST", source, obj.toString(), token);
        JSONObject result = new JSONObject(response).getJSONObject("result");
        return result;
    }

    public int getTotalRecords(String response){
        return new JSONObject(response).getJSONObject("result").getInt("totalRecord");
    }

    public boolean verifyCount(int i, int numOfRecords, String token){
        String key = null;
        String orderStatusName = XLSWorker.getValueFromExcel(i, "SalesOrderStatus", "OrderList");
        switch (orderStatusName){
            case "New":
                key = "numOrderNew";
                break;
            case "ProcessingAll":
                key = "numOrderProccessing";
                break;
            case "Sorted":
                key = "numOrderShipping_DS_Sorted";
                break;
            case "Shipped":
                key = "numOrderShipping_DS_Shipped";
                break;
            case "POD":
                key = "numOrderPod";
                break;
            case "NotPaid":
                key = "numOrderCompleted_PS_Notpaid";
                break;
            case "Complete":
                key = "numOrderCompleted_PS_Complete";
                break;
            case "ReturnSeller":
                key = "numOrderShipping_DS_ReturnSeller";
                break;
            case "ReturnSellerReceived":
                key = "numOrderShipping_DS_ReturnSellerReceived";
                break;
            case "Claim":
                key = "numOrderIsClaim";
                break;
        }
        if(key!=null){
            int numOfOrders = getOrderCount(token).getInt(key);
            if(numOfRecords==numOfOrders)
                return true;
            message = "Order count is not correct! " + numOfRecords + " # " + numOfOrders;
            return false;
        }
        return true;
    }

    public boolean verifyOrderList(int row, JSONObject response){
        String payload = getPayloadFromExcel(row, "OrderList", null);
        JSONObject payloadObj = new JSONObject(payload);
        String carrierCode = (String) payloadObj.get("CarrierCode");
        String orderNumberFrom = (String) payloadObj.get("OrderNumberFrom");

        try{
            JSONArray orders = response.getJSONObject("result").getJSONArray("salesOrders");
            for(int i=0; i<orders.length(); i++){
                JSONObject order = (JSONObject) orders.get(i);
                String orderNumber = (String) order.get("orderNumber");
                if(!orderNumberFrom.equals("") && !orderNumberFrom.equals(orderNumber)){
                    message = "Ordernumber is not matched!";
                    return false;
                }
                String carrierCode2 = (String) order.get("carrierCode");
                if(!carrierCode.equals("") && !carrierCode.equals("ALL") && !carrierCode.equals(carrierCode2)){
                    message = "CarrierCode is not matched!";
                    return false;
                }
            }
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public long getOrderNumberFromCheckout(JSONObject obj){
        return obj.getLong("increment_id");
    }

    public String getOrderNumberFromOrderDetail(JSONObject response){
        return response.getJSONObject("data").getJSONObject("SalesOrder").getString("OrderNumber");
    }

    public JSONObject getOrderObject(JSONObject obj, long orderNumb){
        JSONObject data = obj.getJSONObject("result");
        JSONArray salesorders = data.getJSONArray("salesOrders");
        for(int i=0; i<salesorders.length(); i++){
            JSONObject order = (JSONObject) salesorders.get(i);
            String orderNumber = order.getString("orderNumber");
            if(orderNumber.equals(String.valueOf(orderNumb)))
                return order;
        }
        return null;
    }

    public long getOrderId(JSONObject obj){
        if(obj!=null)
            return obj.getLong("id");
        return -1;
    }

    public String getVersionNo(JSONObject obj){
        if(obj!=null){
            if(obj.has("VersionNo"))
                return obj.getString("VersionNo");
            else
                return obj.getString("versionNo");
        }

        return null;
    }

    public boolean verifyOrderInList(JSONObject obj){
        if(obj!=null)
            return true;
        message = "Cannot find order in order list";
        return false;
    }

    public String getOrderDetail(long orderId, String token){
        JSONObject obj = new JSONObject();
        obj.put("OrderId", orderId);
        String source = getSellerUrl("ENDPOINT_SALESORDER_DETAIL_01");
        return run("POST", source, obj.toString(), token);
    }

    public boolean verifyOrderDetail(long orderId, JSONObject obj){
        if(obj==null)
            return false;
        Long id = obj.getJSONObject("data").getJSONObject("SalesOrder").getLong("Id");
        if(orderId == id)
            return true;
        message = "Load order detail is failed!";
        return false;
    }

    public int getNoteId(JSONObject obj){
        return obj.getJSONObject("data").getInt("Id");
    }

    public String getSalesOrderNotes(int orderId, String token){
        String source = getSellerUrl("ENDPOINT_SALESORDER_GETORDERNOTE");
        JSONObject obj = new JSONObject();
        obj.put("saleOrderId", orderId);
        return run("POST", source, obj.toString(),token);
    }

    public boolean verifyNoteInList(int noteId, JSONObject obj){
        JSONArray data = obj.getJSONArray("data");
        for(int i=0; i<data.length(); i++){
            JSONObject note = (JSONObject) data.get(i);
            int id = note.getInt("Id");
            if(noteId == id)
                return true;
        }
        message = "Cannot load sales order note";
        return false;
    }

    public int getLabelId(JSONObject obj){
        return obj.getJSONObject("result").getInt("id");
    }

    public String getLabelsByOrderId(String orderId, String isActive, String token){
        String source = getSellerUrl("ENDPOINT_SALESORDER_GETLABELLIST") + "?orderId=" + orderId + "&isActive=" + isActive;
        return run("GET", source, "", token);
    }

    public String getLabelsByOrderIdOnMobile(String orderId, String isActive, String token){
        String source = getSellerUrl("ENDPOINT_SALESORDER_MOBILE_GETLABELLIST") + "?orderId=" + orderId + "&isActive=" + isActive;
        return run("GET", source, "", token);
    }

    public boolean verifyLabelInList(int labelId, boolean isActive, JSONObject obj){
        JSONArray smartLabels = obj.getJSONArray("result");
        for(int i=0; i<smartLabels.length(); i++){
            JSONObject label = (JSONObject) smartLabels.get(i);
            int id = label.getInt("id");
            boolean active = label.getBoolean("isActive");
            if(id == labelId && active == isActive){
                return true;
            }
        }
        message = "Add label is failed!";
        return false;
    }

    public boolean deleteLabel(int labelId, String token){
        String source = getSellerUrl("ENDPOINT_SALESORDER_DELETESMARTLABEL") + labelId;
        String response = run("DELETE", source, "", token);
        return response!=null;
    }

    public boolean deleteLabelOnMobile(int labelId, String token){
        String source = getSellerUrl("ENDPOINT_SALESORDER_MOBILE_DELETESMARTLABEL") + labelId;
        String response = run("DELETE", source, "", token);
        return response!=null;
    }

    public boolean verifyLabelNotInList(int labelId, JSONObject obj){
        JSONArray smartLabels = obj.getJSONArray("result");
        for(int i=0; i<smartLabels.length(); i++){
            JSONObject label = (JSONObject) smartLabels.get(i);
            int id = label.getInt("id");
            if(id == labelId){
                message = "Label is still exists!";
                return false;
            }
        }
        return true;
    }

    public boolean exportOrder(int row, String sheetName, String token){
        String source = getSellerUrl(header,"ENDPOINT_SALESORDER_EXPORT");
        source = source + getParamsFromExcel("?", row, sheetName, null);
        String response = run("POST", source, "", token);
        if(response!=null)
            return true;
        message = "Export products is failed!";
        return false;
    }

    public boolean getSOGroup(int row, String sheetName, long orderId, String versionNo, String token){
        String source = getSellerUrl("ENDPOINT_SALESORDER_GETSOGROUP");

        JSONObject obj = new JSONObject();
        obj.put("Id", orderId);
        obj.put("VersionNo", versionNo);
        obj.put("AllowCheck", Boolean.valueOf(XLSWorker.getValueFromExcel(row, "allowCheck", "ConfirmOrder")));
        obj.put("IsDeclareValue", Boolean.valueOf(XLSWorker.getValueFromExcel(row, "IsDeclareValue", "ConfirmOrder")));
        obj.put("IsPostOffice", false);
        obj.put("Page", new JSONObject("{CurrentPage: 1, PageSize: 10}"));
        String response = run("POST", source, obj.toString(), token);
        if(response!=null)
            return true;
        message = "Get SO Group Failed";
        return false;
    }

    public boolean printSalesOrder(String orderId, String token){
        String source = getSellerUrl("ENDPOINT_SALESORDER_PRINTSALESORDER") + orderId;
        String response = run("POST", source, "", token);
        return response.equals("");
    }

    public void updateDeliveryStatus(String orderNumber, String status){
        String source = getSellerUrl("BASE_OMS_URL") + "/api/shipment/update";
        JSONObject obj = new JSONObject();
        obj.put("order_number", orderNumber);
        obj.put("update_time", getCurrentDateTime());
        obj.put("tracking_number", "STGTrackNo" + orderNumber);
        obj.put("actual_weight", 123);
        obj.put("status", status);
        obj.put("comment", "");
        run("POST", source, obj.toString(), "");
    }

}
